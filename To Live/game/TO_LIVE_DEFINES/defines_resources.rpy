﻿init 1:


    #Splashscreen 
    image splash ="00_menu_images/slash.jpg"
    image mal ="00_menu_images/mel.jpg"
    image renpy_cred = "00_menu_images/_menu_renpy.png"
    image warn ="00_menu_images/warn.gif"
    image sunflower ="00_menu_images/sunflowers.png"
    image darkthought ="00_menu_images/startup2.png"
    image dt2 ="00_menu_images/startup 1.png" 
    image placeholder = "00_background/00_placeholder.png"
    define audio.act = "sounds/menu/select_flip.ogg"

    #Beijing
    image Ku_house = "00_background/Ku_house.png"
    image CDX_village = "00_background/art_cuandixia_village_route.png"

    #Location Sprites
    image Beijing_location = "00_location_bookmark/00_Beijing_city.jpg"

    #Sounds

    define audio.sound_menu_logo = "sounds/menu/logo_sound.ogg"

    define audio.ambience_tiben_bells = "sounds/ambience/00_tibetan_bells.wav"
    define audio.ambience_steps = "sounds/ambience/1937_Beijing_ambience_steps.ogg"
    define audio.map_seaside = "sounds/map/ambient_seaside.ogg"

    define audio.chapter_1937 = "sounds/chapter_bookmarks_sounds/1937_sino_japanese_book_mark_sound.ogg"

    define audio.stat_increase = "sounds/stat_increase/00_stat_increase.ogg"

    #Music
    define audio.forgetting_beijing = "music/01_forgetting_beijing.ogg"
    define audio.chenghuangshen_on_the_yangtze = "music/02_chenhuangshen_on_the_yangtze.ogg"
    define audio.the_moons_reflection_on_the_second_spring = "music/03_the_moon_reflection_on_the_second_spring.ogg"
    define audio.guqin_yangguan_sandie = "music/04_Guqin_Yangguan_Sandie.ogg"
    define audio.guqin_zuiyu_changwan = "music/05_Guqin_Zuiyu_Changwan.ogg"
    define audio.art_of_silence = "music/06_art_of_silence_ending.ogg"

    #radio
    define audio.station_bang_chhun_hong = "music/00_radio/01_Bang_Chhun_Hong.ogg"
