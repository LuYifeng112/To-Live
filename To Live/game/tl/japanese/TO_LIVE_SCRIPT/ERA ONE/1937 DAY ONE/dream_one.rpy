﻿# TODO: Translation updated at 2020-06-06 21:00

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:2
translate japanese dream_one_6df873ce:

    # "I feel light."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:3
translate japanese dream_one_3a8af272:

    # "I feel as if there's no weight in me."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:4
translate japanese dream_one_c60970a1:

    # "Like something scraped me from the inside to become hollow."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:5
translate japanese dream_one_ddcc3fc0:

    # "Everything is dark."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:6
translate japanese dream_one_96cd1c83:

    # "But I don't feel alone."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:7
translate japanese dream_one_4972f504:

    # v "Absolution."
    v ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:8
translate japanese dream_one_e7866555:

    # "The voices kept murmuring."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:9
translate japanese dream_one_5d8c382c:

    # "Gossiping about me."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:10
translate japanese dream_one_887e67d3:

    # "I could feel it."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:11
translate japanese dream_one_db38946a:

    # f "It's true,{w=0.5}urchin."
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:12
translate japanese dream_one_19fdc730:

    # f "They all just gossip about you."
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:13
translate japanese dream_one_616f47c1:

    # f "How pathetic you are."
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:14
translate japanese dream_one_3b323e4b:

    # f "How weak you are."
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:15
translate japanese dream_one_de04b0f7:

    # f "You're failure to face reality."
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:16
translate japanese dream_one_4d54a8e5:

    # f "Your desire to run away from your responsibility."
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:17
translate japanese dream_one_46daf088:

    # v "Failure."
    v ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:18
translate japanese dream_one_f80caaf0:

    # f "Did you ever stop and consider anyone else in your life apart from yourself?"
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:21
translate japanese dream_one_7256cf5d:

    # fang "Of course I have!"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:22
translate japanese dream_one_a674a270:

    # f "Saying is one thing."
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:23
translate japanese dream_one_55b8ff4c:

    # f "Doing is another."
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:24
translate japanese dream_one_1b238016:

    # f "Your actions so far only prove your selfishness."
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:27
translate japanese dream_one_58dbfda5:

    # fang "Is there a reason to place something else before my own sense of self?"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:28
translate japanese dream_one_a355c994:

    # f "That is a answer you will discover yourself."
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:29
translate japanese dream_one_dd49be7c:

    # f "I do not exist to guide you."
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:30
translate japanese dream_one_b26351be:

    # f "My reasons for my actions are beyond materialized concepts."
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:31
translate japanese dream_one_9fcc672a:

    # f "The mother pities you urchin."
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:32
translate japanese dream_one_4a512bef:

    # f "I do not."
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:33
translate japanese dream_one_2dbc0370:

    # f "I will remind you of your sins."
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:34
translate japanese dream_one_30901eeb:

    # f "from this day on the choices you make."
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:35
translate japanese dream_one_a0150b86:

    # f "the actions you take"
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:36
translate japanese dream_one_0f5903a0:

    # f "they will be observed."
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:37
translate japanese dream_one_86108569:

    # f "Will you choose to dirty your hands?"
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:38
translate japanese dream_one_6ee6a5fa:

    # f "Will you save and defend people?"
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:39
translate japanese dream_one_ba8df5dc:

    # f "You are alone."
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:40
translate japanese dream_one_a5cd1273:

    # f "You will be alone."
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:41
translate japanese dream_one_5a803fad:

    # f "You will be judged every moment."
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:46
translate japanese dream_one_convo_87d22da6:

    # f "Something you can't comprehend."
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:47
translate japanese dream_one_convo_7fa665cf:

    # f "I may be a part of you."
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:48
translate japanese dream_one_convo_470cfe61:

    # f "An incarnation of your guilt, self hate or frustrations."
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:49
translate japanese dream_one_convo_bdb6f4dc:

    # f "I may be a passing thought."
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:50
translate japanese dream_one_convo_16bcbdad:

    # f "I may be the essence of your character with autonomy."
    f ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:51
translate japanese dream_one_convo_b2df5566:

    # f "You will have to define me."
    f ""

translate japanese strings:

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/dream_one.rpy:43
    old "What are you?"
    new ""

