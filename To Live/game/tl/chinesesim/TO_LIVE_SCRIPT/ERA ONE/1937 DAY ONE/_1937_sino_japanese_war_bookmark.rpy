﻿# TODO: Translation updated at 2020-06-02 14:36

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:7
translate chinesesim Beijing_chapter_one_2d3dbc10:

    # un "It is important you answer these questions properly."
    un ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:8
translate chinesesim Beijing_chapter_one_b4033e09:

    # un "I will go through them one by one."
    un ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:9
translate chinesesim Beijing_chapter_one_e4f0d0a0:

    # un "Do not fret."
    un ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:10
translate chinesesim Beijing_chapter_one_3efd327e:

    # fang "Yes I understand."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:11
translate chinesesim Beijing_chapter_one_125cd8c7:

    # un "Good."
    un ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:12
translate chinesesim Beijing_chapter_one_0deb09cc:

    # un "we're here for you."
    un ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:13
translate chinesesim Beijing_chapter_one_445f30c0:

    # "That thought was calming to me."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:18
translate chinesesim Beijing_chapter_one_c6bf1fd4:

    # "I gulped as I braced myself."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:19
translate chinesesim Beijing_chapter_one_3a49f794:

    # "Times were getting scary."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:20
translate chinesesim Beijing_chapter_one_76053034:

    # "China is on the edge of total war."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:21
translate chinesesim Beijing_chapter_one_21518492:

    # "Uncle Ku Hong-Meng would know what to do."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:22
translate chinesesim Beijing_chapter_one_fd845350:

    # "Professor Po Yeutarng is quite experienced in dealing with wars."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:23
translate chinesesim Beijing_chapter_one_a1110294:

    # "After all they both lived through the Manchurian conquest."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:24
translate chinesesim Beijing_chapter_one_03849d53:

    # "Professor Po Yeutarng volunteered to help guide me what to do based on my life."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:25
translate chinesesim Beijing_chapter_one_6583fd28:

    # "It was a good oppurunity to evaluate my situation and possible choices."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:27
translate chinesesim Beijing_chapter_one_3a9037d4:

    # Po "Are you Ready Fang?"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:34
translate chinesesim Beijing_chapter_one_b26faed2:

    # fang "Please give me a minute to think."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:35
translate chinesesim Beijing_chapter_one_e7df5d21:

    # Po "That's fine,"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:36
translate chinesesim Beijing_chapter_one_7be05b1c:

    # "Normally I would be ready to answer any question coming my way."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:37
translate chinesesim Beijing_chapter_one_3912217c:

    # "I never passed up answering a question."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:38
translate chinesesim Beijing_chapter_one_3fcd3c4f:

    # "Beads of sweat formed on my brow."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:39
translate chinesesim Beijing_chapter_one_614097d8:

    # "Summer had just ended so the heat was burning out like some star."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:40
translate chinesesim Beijing_chapter_one_d2652c32:

    # "I controlled my air-flow and calmed myself down."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:41
translate chinesesim Beijing_chapter_one_f21effee:

    # "I can do this."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:42
translate chinesesim Beijing_chapter_one_3349321c:

    # fang "I am ready."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:43
translate chinesesim Beijing_chapter_one_75b82427:

    # "Uncle Ku looked at me like how a human judges a lamb."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:44
translate chinesesim Beijing_chapter_one_b2f1cc44:

    # Gu "Don't overthink it."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:45
translate chinesesim Beijing_chapter_one_600c74a7:

    # Gu "They won't harm civilians, they didn't back in Tong-pei."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:46
translate chinesesim Beijing_chapter_one_0e044cc5:

    # Gu "Just make sure you can answer their questions."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:50
translate chinesesim Beijing_chapter_one_uncle_interview_7981162e:

    # Po "Well Let us start now."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:51
translate chinesesim Beijing_chapter_one_uncle_interview_adfdd8ca:

    # Po "We have to know more about you to help you plan what to do for the upcoming events."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:52
translate chinesesim Beijing_chapter_one_uncle_interview_7e5dbfb1:

    # Po "War's harsh isn't it?"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:53
translate chinesesim Beijing_chapter_one_uncle_interview_b28b08cd:

    # "Professor Po bit his lip and focussed on the blank piece of paper in front of him."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:54
translate chinesesim Beijing_chapter_one_uncle_interview_5d10b5e0:

    # fang "Yeah it is..."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:55
translate chinesesim Beijing_chapter_one_uncle_interview_c4d78fcd:

    # Po "Lets start off with being realistic."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:56
translate chinesesim Beijing_chapter_one_uncle_interview_0933b80e:

    # Po "Your hometown."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:57
translate chinesesim Beijing_chapter_one_uncle_interview_a5708e11:

    # Po "What is your background in terms of location?"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:58
translate chinesesim Beijing_chapter_one_uncle_interview_0f823fec:

    # Po "I know you and your family have moved places over the years..."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:59
translate chinesesim Beijing_chapter_one_uncle_interview_b641237d:

    # Po "It's best to choose what is your go-to \"home\"."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:60
translate chinesesim Beijing_chapter_one_uncle_interview_480b8fb0:

    # Po "Where is it most convenient for you to return home so you won't have any difficulty resettling or get bothered by Japanese?"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:68
translate chinesesim Beijing_chapter_one_uncle_interview_f64a65aa:

    # fang "I'm from Peiping"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:69
translate chinesesim Beijing_chapter_one_uncle_interview_4ceecee6:

    # Gu "I'd suggest you move out of Peiping."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:70
translate chinesesim Beijing_chapter_one_uncle_interview_272b17bc:

    # Gu "Yan'an is swarming with communists otherwise I would have recommended a temporary stay there."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:71
translate chinesesim Beijing_chapter_one_uncle_interview_602e3327:

    # Gu "My best bet would be to send you to Tsingtao."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:72
translate chinesesim Beijing_chapter_one_uncle_interview_b39a2614:

    # Po "I'd have to be agree with Ku-hsien-sheng"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:73
translate chinesesim Beijing_chapter_one_uncle_interview_9757f7d5:

    # Gu "I have a shcolarly friend who can provide you with oppurtunities for work."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:74
translate chinesesim Beijing_chapter_one_uncle_interview_4e35239f:

    # Gu "Tsingtao will be familiar to you."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:75
translate chinesesim Beijing_chapter_one_uncle_interview_2a919b1d:

    # Po "What do you make of it?"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:76
translate chinesesim Beijing_chapter_one_uncle_interview_4ca4d725:

    # fang "It's a pending decision."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:77
translate chinesesim Beijing_chapter_one_uncle_interview_80524b50:

    # Po "That makes sense."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:78
translate chinesesim Beijing_chapter_one_uncle_interview_02dbef87:

    # Po "As of now both armies are mobilisng so there will be a lot of crossfire here at the inception of the war."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:79
translate chinesesim Beijing_chapter_one_uncle_interview_f95abddc:

    # Po "Back in Da-ren when there was conflict I knew that I could possibly be in danger so I had to act quick."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:80
translate chinesesim Beijing_chapter_one_uncle_interview_563dd877:

    # Po "I buckled down at my boarding school for a few weeks."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:81
translate chinesesim Beijing_chapter_one_uncle_interview_16360afb:

    # Po "We waited and waited as the food came in slower and slower."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:82
translate chinesesim Beijing_chapter_one_uncle_interview_f4d70915:

    # Po "It was scary but we didn't care about that."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:83
translate chinesesim Beijing_chapter_one_uncle_interview_3cc25b1a:

    # Po "Everyone wanted to feel a full stomach again."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:84
translate chinesesim Beijing_chapter_one_uncle_interview_285bacaf:

    # Po "Eventually I snuck out and went to the ports."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:85
translate chinesesim Beijing_chapter_one_uncle_interview_b574e10a:

    # Po "I hoarded some stuff and went back to the school."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:86
translate chinesesim Beijing_chapter_one_uncle_interview_5c25d56b:

    # Po "Anyway I'll tell the rest a little later."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:87
translate chinesesim Beijing_chapter_one_uncle_interview_48d45b74:

    # Po "We have an interview here."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:95
translate chinesesim Beijing_chapter_one_uncle_interview_5a3a4cca:

    # fang "I'm from Nanking"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:96
translate chinesesim Beijing_chapter_one_uncle_interview_f8f78b03:

    # Gu "Well, the capital is a good place."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:97
translate chinesesim Beijing_chapter_one_uncle_interview_254b02d3:

    # Gu "Nationalists would fight to the death to keep it safe."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:98
translate chinesesim Beijing_chapter_one_uncle_interview_ad1ab2c6:

    # Gu "I doubt the Japanese would get that far to Nanking anyway."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:99
translate chinesesim Beijing_chapter_one_uncle_interview_d60980d5:

    # Po "I doubt it too. I can't predict how the war will go, but the capital city will be heavily defended."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:100
translate chinesesim Beijing_chapter_one_uncle_interview_93586bfe:

    # Gu "Your hometown seem to be out of harms way and strife."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:101
translate chinesesim Beijing_chapter_one_uncle_interview_3042fbe9:

    # Po "The only issue is that it's close to Shanghai."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:102
translate chinesesim Beijing_chapter_one_uncle_interview_e7138c55:

    # fang "What's wrong with that?"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:103
translate chinesesim Beijing_chapter_one_uncle_interview_0d146a2d:

    # Po "It's an international settlement area."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:104
translate chinesesim Beijing_chapter_one_uncle_interview_eb59710e:

    # Po "After the Opium wars and the onset of unequal treaties an international zone was established."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:105
translate chinesesim Beijing_chapter_one_uncle_interview_b03936e3:

    # Po "It's under Kuomintang sovereignty but areas of it are allocated to the French, British etc."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:106
translate chinesesim Beijing_chapter_one_uncle_interview_31f1f41f:

    # Po "While the Japanese don't have any legal right that I'm aware of they have an \"unofficial settelemt\" in Kongkew."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:107
translate chinesesim Beijing_chapter_one_uncle_interview_dee41914:

    # Po "It's called \"Little Tokyo\"."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:108
translate chinesesim Beijing_chapter_one_uncle_interview_0f985d2c:

    # Po "That means Shanghai is the gateway to Chinese territory."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:109
translate chinesesim Beijing_chapter_one_uncle_interview_2c026cf5:

    # Po "If you're going to be in Nanking you have to be aware of that."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:110
translate chinesesim Beijing_chapter_one_uncle_interview_23c6bff7:

    # Po "They might as well just leap frog the legal framework of the Shanghai International Zone."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:111
translate chinesesim Beijing_chapter_one_uncle_interview_93e81bb6:

    # Po "War is chaotic and often..."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:112
translate chinesesim Beijing_chapter_one_uncle_interview_3a16f42c:

    # Po "Unpredictable."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:113
translate chinesesim Beijing_chapter_one_uncle_interview_7a77f310:

    # "Uncle Ku has listened with extreme curisoity."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:114
translate chinesesim Beijing_chapter_one_uncle_interview_d665a2bf:

    # "It seemed this was the first time he had learnt about the legal status of Shanghai."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:115
translate chinesesim Beijing_chapter_one_uncle_interview_5b1c9e97:

    # fang "That's so interesting."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:116
translate chinesesim Beijing_chapter_one_uncle_interview_1de4af17:

    # Po "I could be a military strategist with this much analytical skill."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:117
translate chinesesim Beijing_chapter_one_uncle_interview_03a5fbdb:

    # "He gave a dry laugh."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:118
translate chinesesim Beijing_chapter_one_uncle_interview_3f994f98:

    # Po "Anyway lets get going with the questions."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:126
translate chinesesim Beijing_chapter_one_uncle_interview_725562ed:

    # fang "I'm from Kwangchow"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:127
translate chinesesim Beijing_chapter_one_uncle_interview_d4a7a31f:

    # Gu "Under Chen Chi-Tang?"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:128
translate chinesesim Beijing_chapter_one_uncle_interview_2c564340:

    # Gu "I wonder if he can survive or General Chiang will have to bail him out."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:129
translate chinesesim Beijing_chapter_one_uncle_interview_7d1d302f:

    # Gu "I think you're safe there, besides you'll fit in there more that here."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:130
translate chinesesim Beijing_chapter_one_uncle_interview_93586bfe_1:

    # Gu "Your hometown seem to be out of harms way and strife."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:131
translate chinesesim Beijing_chapter_one_uncle_interview_e066f5b6:

    # fang "Guangzhou is under Chiang's direct rule now."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:132
translate chinesesim Beijing_chapter_one_uncle_interview_23c084c3:

    # Gu "Oh really?"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:133
translate chinesesim Beijing_chapter_one_uncle_interview_e65ff6a7:

    # Po "Didn't he take control about a year ago?"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:134
translate chinesesim Beijing_chapter_one_uncle_interview_71aa2917:

    # Po "I'm not so up to date with that."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:135
translate chinesesim Beijing_chapter_one_uncle_interview_11e9f7c8:

    # fang "Yes that's true."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:136
translate chinesesim Beijing_chapter_one_uncle_interview_f2e97a4e:

    # Po "The south is diverse isn't it?"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:137
translate chinesesim Beijing_chapter_one_uncle_interview_7167f3ac:

    # "Porfessor Po has this rhetoric that made him interesting to listen to."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:138
translate chinesesim Beijing_chapter_one_uncle_interview_b2444729:

    # Po "Can you speak Cantonese?"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:139
translate chinesesim Beijing_chapter_one_uncle_interview_c9d3281d:

    # fang "Yes I can."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:140
translate chinesesim Beijing_chapter_one_uncle_interview_a9ca1a43:

    # Po "I think it's a beautiful language."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:141
translate chinesesim Beijing_chapter_one_uncle_interview_78af48f6:

    # "Uncle Ku made a smirk."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:142
translate chinesesim Beijing_chapter_one_uncle_interview_4672a245:

    # Gu "No offense Po-Hsian-Sheng, I prefer Mandarin."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:143
translate chinesesim Beijing_chapter_one_uncle_interview_d527419d:

    # "Professor Po flashed a surprised face."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:144
translate chinesesim Beijing_chapter_one_uncle_interview_1de9208a:

    # Po "How so?"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:145
translate chinesesim Beijing_chapter_one_uncle_interview_165bb866:

    # Gu "In Cantonese there are many more tones."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:146
translate chinesesim Beijing_chapter_one_uncle_interview_e35f5168:

    # Gu "Mandarin only has four."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:147
translate chinesesim Beijing_chapter_one_uncle_interview_7a01bdb0:

    # Gu "I also believe in focussing on a vernacular language."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:148
translate chinesesim Beijing_chapter_one_uncle_interview_74cac819:

    # Po "Acceptable views I guess."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:149
translate chinesesim Beijing_chapter_one_uncle_interview_d5122e05:

    # Po "But I don't realise how these points prove Cantonese is not a beautiful language."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:150
translate chinesesim Beijing_chapter_one_uncle_interview_eac8c6b4:

    # Po "The diveristy in tones make it much more rhythmic."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:151
translate chinesesim Beijing_chapter_one_uncle_interview_13179f9d:

    # Gu "To each their own."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:152
translate chinesesim Beijing_chapter_one_uncle_interview_0fa1ddaa:

    # Po "Glad to disagree Ku-hsien-sheng"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:153
translate chinesesim Beijing_chapter_one_uncle_interview_35d18710:

    # "Professor Po flashed a smile and Uncle Ku teasignly."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:154
translate chinesesim Beijing_chapter_one_uncle_interview_3346e42e:

    # Gu "Let's get on with the questions."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:162
translate chinesesim Beijing_chapter_one_uncle_interview_1e35d872:

    # fang "I'm from Hong-Kong"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:163
translate chinesesim Beijing_chapter_one_uncle_interview_9231bd50:

    # Po "It is a good place."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:164
translate chinesesim Beijing_chapter_one_uncle_interview_bf3dcb97:

    # Po "The British may be a bit foreign to us mainlanders but at least their territories aren't at risk of being involved in a war."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:165
translate chinesesim Beijing_chapter_one_uncle_interview_93586bfe_2:

    # Gu "Your hometown seem to be out of harms way and strife."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:166
translate chinesesim Beijing_chapter_one_uncle_interview_9f8a99d8:

    # Po "I take it you are fluent in English?"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:167
translate chinesesim Beijing_chapter_one_uncle_interview_220093ab:

    # fang "Yes, that is true."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:168
translate chinesesim Beijing_chapter_one_uncle_interview_98093170:

    # Po "Do you read the bible in Cantonese as well?"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:169
translate chinesesim Beijing_chapter_one_uncle_interview_84ad2450:

    # fang "Yes we have it in many languages."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:170
translate chinesesim Beijing_chapter_one_uncle_interview_eb41d97b:

    # Po "It's nice to learn about the other places in this country."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:171
translate chinesesim Beijing_chapter_one_uncle_interview_27564383:

    # Po "It's so diverse."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:172
translate chinesesim Beijing_chapter_one_uncle_interview_ea7c9fc5:

    # Po "Each place seemes to teach you something new."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:173
translate chinesesim Beijing_chapter_one_uncle_interview_fdd0f819:

    # "I seem to agree with it also."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:174
translate chinesesim Beijing_chapter_one_uncle_interview_5ce31e67:

    # "Alot of cities in China are pockets of intenrational influence from Hong-Kong to Macau to Shanghai etc."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:175
translate chinesesim Beijing_chapter_one_uncle_interview_4745b5e6:

    # "At times two people in the same country feel alien to each other."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:176
translate chinesesim Beijing_chapter_one_uncle_interview_b09edfba:

    # "No wonder there's so much divisions in our society."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:177
translate chinesesim Beijing_chapter_one_uncle_interview_a88b3656:

    # "So many warlords and independant states."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:178
translate chinesesim Beijing_chapter_one_uncle_interview_d463037e:

    # Po "Fang!"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:179
translate chinesesim Beijing_chapter_one_uncle_interview_9990c1d7:

    # "I sat up qucikly and faced Professor Po."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:180
translate chinesesim Beijing_chapter_one_uncle_interview_3d7a7b32:

    # Po "You dozed out."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:182
translate chinesesim Beijing_chapter_one_uncle_interview_cbef447f:

    # fang "Seems to be a unending habit."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:183
translate chinesesim Beijing_chapter_one_uncle_interview_1459168b:

    # Po "you'll grow out of it I'm sure."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:184
translate chinesesim Beijing_chapter_one_uncle_interview_e0f7a843:

    # Po "I wanted to go to the next question."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:192
translate chinesesim Beijing_chapter_one_uncle_interview_f14847bf:

    # fang "I'm from Macau"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:193
translate chinesesim Beijing_chapter_one_uncle_interview_ba28c024:

    # Gu "A place that won't get thrashed by war."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:194
translate chinesesim Beijing_chapter_one_uncle_interview_2a8f6958:

    # Gu "Portugese don't involve themselves in wars."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:195
translate chinesesim Beijing_chapter_one_uncle_interview_3a020593:

    # Gu "Plus they let Chiense culture flourish unlike the Guizi in Hong Kong."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:196
translate chinesesim Beijing_chapter_one_uncle_interview_0406ad44:

    # "Uncle Ku has a strong bias for Macao, he disliked how the British ran Hong Kong and prefered Portugese ruled Macao."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:197
translate chinesesim Beijing_chapter_one_uncle_interview_9c6772bb:

    # Gu "Some people are born lucky I guess."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:198
translate chinesesim Beijing_chapter_one_uncle_interview_1d27846f:

    # Po "Will you go ahead and start gambling?"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:199
translate chinesesim Beijing_chapter_one_uncle_interview_0bc2060a:

    # Po "Gambling is like water in Macao. A {i}necessity{/i}."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:200
translate chinesesim Beijing_chapter_one_uncle_interview_6c3a3c86:

    # "Professor Po gave a laugh about it."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:201
translate chinesesim Beijing_chapter_one_uncle_interview_57c334fb:

    # "Uncle Gu did not approve of gambling but laughed at this joke regardless."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:202
translate chinesesim Beijing_chapter_one_uncle_interview_5f3e6b25:

    # Gu "I think it is a safe place that you should consider returning to."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:203
translate chinesesim Beijing_chapter_one_uncle_interview_93586bfe_3:

    # Gu "Your hometown seem to be out of harms way and strife."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:204
translate chinesesim Beijing_chapter_one_uncle_interview_fcd6b284:

    # Gu "I suggest you go back to your family and try to prepare what to do next."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:212
translate chinesesim Beijing_chapter_one_uncle_interview_aca559de:

    # fang "I'm from Taiwan"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:213
translate chinesesim Beijing_chapter_one_uncle_interview_f30014a9:

    # Gu "From the foreign devils themself."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:214
translate chinesesim Beijing_chapter_one_uncle_interview_7c978644:

    # "He chuckles"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:215
translate chinesesim Beijing_chapter_one_uncle_interview_beecc522:

    # Gu "They won't mess with you then i guess."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:219
translate chinesesim Beijing_chapter_one_uncle_interview_intention_b0fd8a53:

    # Po "So what are you doing in Peiping right now?"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:228
translate chinesesim Beijing_chapter_one_uncle_interview_intention_4b858c87:

    # "{font=fonts/eng_octin_spraypaint/octin spraypaint a rg.ttf}{color=#4fc1ff}Fang gained 3 Determination; 3 Intellect and 2 Dexterity!{/color}{/font}"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:229
translate chinesesim Beijing_chapter_one_uncle_interview_intention_42d40347:

    # fang "I came here to actually continue my education."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:230
translate chinesesim Beijing_chapter_one_uncle_interview_intention_08466030:

    # fang "I am staying with Uncle Ku since it is close to the facilities."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:231
translate chinesesim Beijing_chapter_one_uncle_interview_intention_0b34abc7:

    # Po "I doubt schools will run under foreign occupation."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:232
translate chinesesim Beijing_chapter_one_uncle_interview_intention_a9e421bb:

    # "Professor Po gave a faint grin."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:233
translate chinesesim Beijing_chapter_one_uncle_interview_intention_fbd4821d:

    # Po "Well classes won't."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:234
translate chinesesim Beijing_chapter_one_uncle_interview_intention_4f933af7:

    # "Professor Po raised his glass of {i}Máotái{/i} and took a sip."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:241
translate chinesesim Beijing_chapter_one_uncle_interview_intention_b2045dbd:

    # "He normally didn't drink heavily but Uncle Ku had handed him this as a token of thanks for visiting me."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:242
translate chinesesim Beijing_chapter_one_uncle_interview_intention_1000e281:

    # "Giving such an expensive drink symbolised Uncle Ku's gratitude."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:243
translate chinesesim Beijing_chapter_one_uncle_interview_intention_860ae260:

    # Po "Most students back in Dongbei would just live at the boarding school."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:244
translate chinesesim Beijing_chapter_one_uncle_interview_intention_ca3e78a4:

    # Po "They would not return to their families."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:245
translate chinesesim Beijing_chapter_one_uncle_interview_intention_34a834fe:

    # "He raised the cup of {i}Máotái{/i} to his lips but didn't take a sip. {w}As if something important crossed his mind.{w=1.2}"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:246
translate chinesesim Beijing_chapter_one_uncle_interview_intention_f822f239:

    # Po "After the first month a few homesick students did in fact return."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:247
translate chinesesim Beijing_chapter_one_uncle_interview_intention_1db3a19a:

    # Po "No vandalism, no abusive powers and the old deposed emperor back on a throne."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:248
translate chinesesim Beijing_chapter_one_uncle_interview_intention_ee15d83a:

    # Po "Most parents made kids work back on the farms or their old shanty businesses while the Japanese and {i}Kang-te Pu Yi{/i} tried to get the old ways running again."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:249
translate chinesesim Beijing_chapter_one_uncle_interview_intention_02a84750:

    # "I could make out the sorrow in his face.{nw}"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:250
translate chinesesim Beijing_chapter_one_uncle_interview_intention_70ad8da6:

    # "It seemed he made himself remember something he wished to forget."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:251
translate chinesesim Beijing_chapter_one_uncle_interview_intention_2410257a:

    # Po "So I guess you should head back."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:252
translate chinesesim Beijing_chapter_one_uncle_interview_intention_a6dfca98:

    # Po "I think your family will need you more."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:253
translate chinesesim Beijing_chapter_one_uncle_interview_intention_364c73ce:

    # "He looked down at his papers as he dipped the brush back into a clay pot of ink and scribed some details on a creased paper."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:261
translate chinesesim Beijing_chapter_one_uncle_interview_intention_4ddb793f:

    # "{font=fonts/eng_octin_spraypaint/octin spraypaint a rg.ttf}{color=#4fc1ff}Fang gained 3 Intellect and 3 Dexterity!{/color}{/font}"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:262
translate chinesesim Beijing_chapter_one_uncle_interview_intention_51229248:

    # fang "I am working as an apprentice to my Uncle."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:263
translate chinesesim Beijing_chapter_one_uncle_interview_intention_b1231e59:

    # Gu "The kid is quite dexterous."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:264
translate chinesesim Beijing_chapter_one_uncle_interview_intention_580a465e:

    # Gu "It's hard to believe he wasn't born without some miracle jade in the mouth story."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:265
translate chinesesim Beijing_chapter_one_uncle_interview_intention_87dab2b1:

    # Po "Comments like that will get you in high places."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:266
translate chinesesim Beijing_chapter_one_uncle_interview_intention_1c0f9040:

    # fang "Thank you"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:267
translate chinesesim Beijing_chapter_one_uncle_interview_intention_d5398ea9:

    # Po "anyway..."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:276
translate chinesesim Beijing_chapter_one_uncle_interview_intention_1f43de87:

    # "{font=fonts/eng_octin_spraypaint/octin spraypaint a rg.ttf}{color=#4fc1ff}Fang gained 2 determination!{/color}{/font}"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:277
translate chinesesim Beijing_chapter_one_uncle_interview_intention_fa81d2fd:

    # Po "what a hardworking kid."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:278
translate chinesesim Beijing_chapter_one_uncle_interview_intention_f113ba68:

    # Gu "I agree."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:279
translate chinesesim Beijing_chapter_one_uncle_interview_intention_30fff828:

    # "Uncle Ku nodded with professor Po."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:280
translate chinesesim Beijing_chapter_one_uncle_interview_intention_80f86172:

    # "They had seemed to be harmonious in their actions.. {w}{cps=*0.5}Like they were parts of the same piece...{/cps}{w=1.2}"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:281
translate chinesesim Beijing_chapter_one_uncle_interview_intention_c62084cf:

    # Gu "This kind of attitude sculpts heroes and leaders."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:282
translate chinesesim Beijing_chapter_one_uncle_interview_intention_9a258bb7:

    # Po "You have probably saved enough now. Have you?"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:283
translate chinesesim Beijing_chapter_one_uncle_interview_intention_a54a105e:

    # "Working at Cheung's silkhouse paid off well. It still does"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:284
translate chinesesim Beijing_chapter_one_uncle_interview_intention_379c436b:

    # "This summer I had saved up enough to return home again."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:285
translate chinesesim Beijing_chapter_one_uncle_interview_intention_25f5d2b0:

    # fang "yes, it is sufficient I would say."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:286
translate chinesesim Beijing_chapter_one_uncle_interview_intention_a844b18f:

    # Po "That is great to hear"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:297
translate chinesesim Beijing_chapter_one_uncle_interview_intention_6b652316:

    # "{font=fonts/eng_octin_spraypaint/octin spraypaint a rg.ttf}{color=#4fc1ff}Fang gained 3 strength; 4 determination; 4 dexterity!{/color}{/font}"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:298
translate chinesesim Beijing_chapter_one_uncle_interview_intention_846f89f2:

    # fang "I'm taking a break from military conscription training."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:299
translate chinesesim Beijing_chapter_one_uncle_interview_intention_7a577177:

    # Po "How come you weren't enlisted?"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:300
translate chinesesim Beijing_chapter_one_uncle_interview_intention_51096e9d:

    # "Professor Po looks confused as he asks me this question."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:301
translate chinesesim Beijing_chapter_one_uncle_interview_intention_d9268ef3:

    # fang "I'm only sixteen years old as of now. I can be trained but I can't be enlisted to fight already."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:302
translate chinesesim Beijing_chapter_one_uncle_interview_intention_8a711f57:

    # Po "Your time of birth is lucky I guess."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:303
translate chinesesim Beijing_chapter_one_uncle_interview_intention_573d339c:

    # fang "I guess,{w}others were rounded up forcefully and marched to camps in chains."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:304
translate chinesesim Beijing_chapter_one_uncle_interview_intention_8368484b:

    # "I turn my head towards Uncle Ku"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:305
translate chinesesim Beijing_chapter_one_uncle_interview_intention_3fadb12e:

    # fang "Uncle Ku paid off the recruiters."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:306
translate chinesesim Beijing_chapter_one_uncle_interview_intention_f9e87276:

    # "He gives a smile back."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:307
translate chinesesim Beijing_chapter_one_uncle_interview_intention_4e4162f5:

    # fang "Thanks to him I don't have to become a war child."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:308
translate chinesesim Beijing_chapter_one_uncle_interview_intention_601957c2:

    # "Professor Po turns towards Uncle Ku as well."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:309
translate chinesesim Beijing_chapter_one_uncle_interview_intention_42b05a8d:

    # Po "{i}Pèifú{/i}{nw}"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:312
translate chinesesim Beijing_chapter_one_uncle_interview_intention_7a11fa82:

    # "He had raised his {i}Máotái{/i} with both hands to show his appreciation."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:313
translate chinesesim Beijing_chapter_one_uncle_interview_intention_a02c5a92:

    # "You should stay away from the military. {w}You're too young for that life.{w=1.0}"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:314
translate chinesesim Beijing_chapter_one_uncle_interview_intention_117c2ee8:

    # Po "You're also too young to watch me drink {i}Máotái{/i}"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:315
translate chinesesim Beijing_chapter_one_uncle_interview_intention_3bcd76dd:

    # "He gave a hearty chuckle. His face was red from the alcohol."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:316
translate chinesesim Beijing_chapter_one_uncle_interview_intention_fbc7a589:

    # "He dipped his brush into the little clay pot of abyss-like ink."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:317
translate chinesesim Beijing_chapter_one_uncle_interview_intention_f4494580:

    # "You couldn't see it spin like water. It was fascinating."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:318
translate chinesesim Beijing_chapter_one_uncle_interview_intention_d8538bb1:

    # "He removed the brush and began to scribe onto his piece of paper once again."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:319
translate chinesesim Beijing_chapter_one_uncle_interview_intention_be88bbd4:

    # Po "I will lie and state that you were discharged by injury."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:323
translate chinesesim Beijing_chapter_one_uncle_interview_religion_ae73c56f:

    # Po "What belief system do you follow?"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:328
translate chinesesim Beijing_chapter_one_uncle_interview_religion_c4ab0ea2:

    # Po "You seem to be clinging on the old ways."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:329
translate chinesesim Beijing_chapter_one_uncle_interview_religion_37791e4c:

    # "Professor Po sipped his {i}Máotái{/i} lightly before facing me."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:330
translate chinesesim Beijing_chapter_one_uncle_interview_religion_5ca5df6d:

    # "Bits of his face had manifested a gradient of red."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:331
translate chinesesim Beijing_chapter_one_uncle_interview_religion_ad22454c:

    # Po "I guess the Kuomintang did a good job reinforcing tradition."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:332
translate chinesesim Beijing_chapter_one_uncle_interview_religion_e74da966:

    # "He sipped one again. {w}{cps=*0.5}As if something was annoying him inside{/cps}."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:333
translate chinesesim Beijing_chapter_one_uncle_interview_religion_9490f8d6:

    # Po "Confucianism is a very old principle."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:336
translate chinesesim Beijing_chapter_one_uncle_interview_religion_fc776471:

    # Po "I...{w}{cps=*2}just don't see it going anywhere in the modern era.{/cps}"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:337
translate chinesesim Beijing_chapter_one_uncle_interview_religion_4c68cb64:

    # "Uncle Ku looked at him strangely."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:338
translate chinesesim Beijing_chapter_one_uncle_interview_religion_4cc742e0:

    # Po "The \"New Life movement\" is an illusion."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:339
translate chinesesim Beijing_chapter_one_uncle_interview_religion_d0a4be69:

    # Po "I am not a \"communist\" {w}but I don't like the policies of the nationalists either."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:340
translate chinesesim Beijing_chapter_one_uncle_interview_religion_9e5bf53e:

    # "Professor Po seemed engrossed with his political beliefs."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:341
translate chinesesim Beijing_chapter_one_uncle_interview_religion_5d0a560f:

    # Po "I respect your beliefs {i}Xiǎo Fāng{/i} but...{nw}"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:342
translate chinesesim Beijing_chapter_one_uncle_interview_religion_c9638a51:

    # Po "From a philosophical and political doctrine I dislike the state being an obstacle to modernism."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:343
translate chinesesim Beijing_chapter_one_uncle_interview_religion_8a4c0e88:

    # Gu "I could agree with that.{nw}"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:344
translate chinesesim Beijing_chapter_one_uncle_interview_religion_c4f5f6d4:

    # Gu "In english they say something along the lines of \"seperation of church and state\"."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:345
translate chinesesim Beijing_chapter_one_uncle_interview_religion_e1a1602b:

    # Gu "Still, I persist, I believe in Confucianism."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:346
translate chinesesim Beijing_chapter_one_uncle_interview_religion_eecbba54:

    # Gu "It worked for 2000 years why can't it work for another 2000?"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:347
translate chinesesim Beijing_chapter_one_uncle_interview_religion_a7a93ef6:

    # "Uncle Ku had seemed to have taken offence to Professor Po's anti-Confucian stance."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:348
translate chinesesim Beijing_chapter_one_uncle_interview_religion_b2e3eaf5:

    # fang "I have a neutral atttiude to Confucianism."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:349
translate chinesesim Beijing_chapter_one_uncle_interview_religion_fba4572b:

    # fang "I think some things work and some do not."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:350
translate chinesesim Beijing_chapter_one_uncle_interview_religion_8ef3903f:

    # fang "Fillial Piety is one of my core beliefs."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:351
translate chinesesim Beijing_chapter_one_uncle_interview_religion_e2fb5818:

    # "Prossor Po smirked."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:352
translate chinesesim Beijing_chapter_one_uncle_interview_religion_89dc7d83:

    # Po "The things you say become more interesting as I get more drunk."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:353
translate chinesesim Beijing_chapter_one_uncle_interview_religion_0c3c6a30:

    # "Professor Po seemed to dip his brush more sloppily and scribe less cautiously."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:354
translate chinesesim Beijing_chapter_one_uncle_interview_religion_57749235:

    # "I wondered if he was capable of asking another question."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:355
translate chinesesim Beijing_chapter_one_uncle_interview_religion_cfd40bfb:

    # "I hope he would have enough capaicity to finish to interview before he became too drunk."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:356
translate chinesesim Beijing_chapter_one_uncle_interview_religion_cb0fd413:

    # "I wanted to finish this quickly for the sake of decision making."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:357
translate chinesesim Beijing_chapter_one_uncle_interview_religion_8efa847b:

    # "Every second I'm here feels more dangerous."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:358
translate chinesesim Beijing_chapter_one_uncle_interview_religion_ec941ab9:

    # "Perhaps I', prone to overthinking but I just wanted to leave Peiping."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:359
translate chinesesim Beijing_chapter_one_uncle_interview_religion_7bee335b:

    # "I don't have the strength to persist through violence."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:360
translate chinesesim Beijing_chapter_one_uncle_interview_religion_d463037e:

    # Po "Fang!"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:362
translate chinesesim Beijing_chapter_one_uncle_interview_religion_17ef5589:

    # Po "You dozed off again."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:363
translate chinesesim Beijing_chapter_one_uncle_interview_religion_67a9e76b:

    # Po "I used to be like that too."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:364
translate chinesesim Beijing_chapter_one_uncle_interview_religion_d2b5d51c:

    # Po "I think you overthink a bit don't you?"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:365
translate chinesesim Beijing_chapter_one_uncle_interview_religion_e42e551b:

    # Po "You need to calm down."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:366
translate chinesesim Beijing_chapter_one_uncle_interview_religion_a0678f30:

    # Po "This was you can think better and clearly."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:367
translate chinesesim Beijing_chapter_one_uncle_interview_religion_b383eb17:

    # Po "Hence decision making will be easier."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:369
translate chinesesim Beijing_chapter_one_uncle_interview_religion_6d3f1e1c:

    # Po "Stay alert a litte bit longer then you go think about whatever you want."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:370
translate chinesesim Beijing_chapter_one_uncle_interview_religion_01bfacaa:

    # Po "I'm doing my best as well."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:371
translate chinesesim Beijing_chapter_one_uncle_interview_religion_acab1efc:

    # Po "Let's keep going."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:372
translate chinesesim Beijing_chapter_one_uncle_interview_religion_07b61d64:

    # Gu "Wait,"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:376
translate chinesesim Beijing_chapter_one_uncle_interview_religion_be1bc4b9:

    # Po "Ah...{w}{cps=*0.9}A fellow buddhist.{/cps}"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:377
translate chinesesim Beijing_chapter_one_uncle_interview_religion_37791e4c_1:

    # "Professor Po sipped his {i}Máotái{/i} lightly before facing me."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:378
translate chinesesim Beijing_chapter_one_uncle_interview_religion_5ca5df6d_1:

    # "Bits of his face had manifested a gradient of red."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:379
translate chinesesim Beijing_chapter_one_uncle_interview_religion_aa8b3b04:

    # Po "Have you ever heard of {i}Dhammapada{/i}? {w}{cps=*1.5}the poem of Buddha awakening?{/cps}"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:384
translate chinesesim Beijing_chapter_one_uncle_interview_religion_03059a99:

    # Po "You being from Hong Kong I wouldn't be suprised at that perspective."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:385
translate chinesesim Beijing_chapter_one_uncle_interview_religion_6cfe25a3:

    # Po "Do you like listening to hymns?"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:386
translate chinesesim Beijing_chapter_one_uncle_interview_religion_f70df039:

    # "Professor Po held his {i}Máotái{/i} up to the sunlight where he could see it tinker."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:387
translate chinesesim Beijing_chapter_one_uncle_interview_religion_2e81d903:

    # "I nodded back towards him."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:388
translate chinesesim Beijing_chapter_one_uncle_interview_religion_99c5ec88:

    # Po "I love latin choir and hymns."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:389
translate chinesesim Beijing_chapter_one_uncle_interview_religion_e4b4246e:

    # Po "It just moves me to the core."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:392
translate chinesesim taoism_breakaway_40409a9c:

    # Gu "{i}Fang{/i}"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:393
translate chinesesim taoism_breakaway_5394457a:

    # Gu "Did you read the old poems from {i}\"Tao Te Ch'ing\"{/i}?"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:396
translate chinesesim taoism_breakaway_15492381:

    # Gu "Would you like to hear a poem from {i}Tao Te Ch'ing{/i}?"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:397
translate chinesesim taoism_breakaway_02aaa657:

    # Gu "{i}Tao the water way{/i}"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:400
translate chinesesim taoism_breakaway_8ba10ff4:

    # fang "Yes I would like to hear it."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:401
translate chinesesim taoism_breakaway_68d00552:

    # "Professor Po was now heavily intoxicated and slumping in his chair as he strained to keep focus."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:402
translate chinesesim taoism_breakaway_2427be3c:

    # Gu "I don't think he will ming me reciting a poem of backwards tradition."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:403
translate chinesesim taoism_breakaway_ec76b293:

    # "Uncle Ku smirked at Po."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:404
translate chinesesim taoism_breakaway_19968040:

    # Gu "{i}The best of men is like water;{/i}"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:405
translate chinesesim taoism_breakaway_df0331cb:

    # Gu "{i}Water benefits all things{/i}"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:406
translate chinesesim taoism_breakaway_0545f838:

    # Gu "{i}And does not compete with them.{/i}"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:407
translate chinesesim taoism_breakaway_39721a32:

    # Gu "{i}It dwells in (the lowly) places that all disdain –{/i}"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:408
translate chinesesim taoism_breakaway_f0f55c7c:

    # Gu "{i}Wherein it comes near to the Tao.{/i}"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:409
translate chinesesim taoism_breakaway_3e775767:

    # Gu "{i}In his dwelling, (the Sage) loves the (lowly) earth;{/i}"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:410
translate chinesesim taoism_breakaway_934224d0:

    # Gu "{i}In his heart, he loves what is profound;{/i}"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:411
translate chinesesim taoism_breakaway_68d13ef3:

    # Gu "{i}In his relations with others, he loves kindness;{/i}"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:412
translate chinesesim taoism_breakaway_60b6999f:

    # Gu "{i}In his words, he loves sincerity;{/i}"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:413
translate chinesesim taoism_breakaway_65936043:

    # Gu "{i}In government, he loves peace;{/i}"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:414
translate chinesesim taoism_breakaway_76a257f8:

    # Gu "{i}In business affairs, he loves ability;{/i}"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:415
translate chinesesim taoism_breakaway_c9751fdb:

    # Gu "{i}In his actions, he loves choosing the right time.{/i}"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:416
translate chinesesim taoism_breakaway_e1e27749:

    # Gu "{i}It is because he does not contend{/i}"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:417
translate chinesesim taoism_breakaway_3bd5048e:

    # Gu "{i}That he is without reproach.{/i}"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:420
translate chinesesim taoism_breakaway_a14ee08e:

    # fang "That's deep."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:421
translate chinesesim taoism_breakaway_bde86d7e:

    # Gu "It's life."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:422
translate chinesesim taoism_breakaway_f5c06c99:

    # Gu "I noticed that you overthink a lot."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:423
translate chinesesim taoism_breakaway_4309068a:

    # Gu "You have to be able to understand yourself if you want to be effecient."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:424
translate chinesesim taoism_breakaway_8ef6abf8:

    # Gu "So you have to understand your feelings and learn how to protect yourself from bottling up or lashing out."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:425
translate chinesesim taoism_breakaway_7d8f6e70:

    # fang "Thanks for that."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:426
translate chinesesim taoism_breakaway_517ff47c:

    # Gu "I think everyone needs some calming pep talk."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:427
translate chinesesim taoism_breakaway_537dbb19:

    # fang "I couln't agree less."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:428
translate chinesesim taoism_breakaway_4cbdf80f:

    # "I looked over to Professor Po who didn't have the strength to clutch his cup properly."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:432
translate chinesesim taoism_breakaway_ad0d774d:

    # fang "I think maybe later.. {w=1}When this interview is finished."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:433
translate chinesesim taoism_breakaway_9fed2262:

    # Po "The boy knows his priorities."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:434
translate chinesesim taoism_breakaway_1adfa345:

    # "Professor Po was begining to talk more openly and slurredly."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:435
translate chinesesim taoism_breakaway_8adece60:

    # fang "Let's go to the next question."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:438
translate chinesesim taoism_breakaway_dfd4dae5:

    # fang "I already know the poem."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:445
translate chinesesim buddhism_breakaway_7356fae3:

    # Po "Well that's great!"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:446
translate chinesesim buddhism_breakaway_9dfa09c6:

    # Po "There are so many out there."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:447
translate chinesesim buddhism_breakaway_d838c6ee:

    # "Professor Po was beginning to look like he had too much to drink."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:452
translate chinesesim buddhism_breakaway_dca5c439:

    # Po "It is a great poem."
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:453
translate chinesesim buddhism_breakaway_ff6de641:

    # "Professor Po had becoem slightly{cps=*0.2}...{/cps} {w=1}intoxicated by this point."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:454
translate chinesesim buddhism_breakaway_fe504d99:

    # "Despite that it seemed that Professor Po was sober enough to recite Buddhist poetry."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:455
translate chinesesim buddhism_breakaway_7c34c78a:

    # Po "{i}{cps=*0.4}Through the round of many births I roamed{/cps}{/i}{nw}"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:456
translate chinesesim buddhism_breakaway_0bbe0754:

    # "Professor Po had in fact placed his {i}{font=fonts/chi_pinyin/cpinyin0.ttf}Máotái{/font}{/i} down as he recited this."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:457
translate chinesesim buddhism_breakaway_aca211b3:

    # Po "{i}{cps=*0.4}without reward,{w=2} without rest,{/cps}{/i}{nw}"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:458
translate chinesesim buddhism_breakaway_7218e54e:

    # Po "{i}{cps=*0.4}seeking the house-builder.{/cps}{/i}{nw}"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:459
translate chinesesim buddhism_breakaway_16cbb3a6:

    # Po "{i}{cps=*0.4}Painful is birth{/cps}{/i}{nw}"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:460
translate chinesesim buddhism_breakaway_c1af2be9:

    # Po "{i}{cps=*0.4}again & again.{/cps}{/i}{nw}"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:461
translate chinesesim buddhism_breakaway_f596b8af:

    # Po "{i}{cps=*0.4}House-builder, you're seen!{/cps}{/i}{nw}"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:462
translate chinesesim buddhism_breakaway_180072d3:

    # Po "{i}{cps=*0.4}You will not build a house again.{/cps}{/i}{nw}"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:463
translate chinesesim buddhism_breakaway_a4e4fd71:

    # Po "{i}{cps=*-0.4}All your rafters broken,{/cps}{/i}{nw}"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:464
translate chinesesim buddhism_breakaway_f36b94b4:

    # Po "{i}{cps=*0.4}the ridge pole destroyed,{/cps}{/i}{nw}"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:465
translate chinesesim buddhism_breakaway_999c9925:

    # Po "{i}{cps=*0.4}gone to the Unformed{/cps}{cps=*0.2}...{/cps}{/i}{nw}"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:466
translate chinesesim buddhism_breakaway_8d58f6ad:

    # Po "{i}{cps=*0.4}the mind has come to the end of craving.{/cps}{/i}{nw}"
    Po ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:469
translate chinesesim buddhism_breakaway_15f35a52:

    # "Professor seemed to become internalized during the recital of this poem."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:470
translate chinesesim buddhism_breakaway_f54e22be:

    # "In his red intoxicated face, you could see his expression of grattitude and servility."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:474
translate chinesesim beijing_interview_after_2e6f64c7:

    # "Professor Po seemed to increasingly become unable to stay...{w=1.0}sober."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:475
translate chinesesim beijing_interview_after_f873d361:

    # Gu "He might need to take some rest."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:476
translate chinesesim beijing_interview_after_c66a74f6:

    # Gu "He grew up around the drink."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:477
translate chinesesim beijing_interview_after_11928383:

    # "Uncle Ku grabbed Professor Po by his arms and mounted him on his back."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:478
translate chinesesim beijing_interview_after_3ae3ee89:

    # "Professor Po only slurred some words as Uncle Ku dragged him to a guest room."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:479
translate chinesesim beijing_interview_after_9208d330:

    # "I guess{w=1.0} the interview will be conducted later."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:480
translate chinesesim beijing_interview_after_6f6cd1fd:

    # "I turned my head around to the Taoist Shrine."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:481
translate chinesesim beijing_interview_after_e2b27ec8:

    # "It consisted of a {i}Yīnyáng{/i} emblem,{w=0.5} A candle that possessed a mediocre flame,{w=0.5} and a copy of {i}Tao Te Ch'ing{/i}."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:482
translate chinesesim beijing_interview_after_2291056b:

    # "I stood up from the uncomfortable chair and approached the shrine."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:483
translate chinesesim beijing_interview_after_07853f15:

    # "It was peaceful to stand before the shrine."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:484
translate chinesesim beijing_interview_after_1d1c4241:

    # "I wasn't required to announce my presence."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:485
translate chinesesim beijing_interview_after_d14a4cec:

    # "Part of me was afraid of the new world I had stepped into."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:486
translate chinesesim beijing_interview_after_7b2ac531:

    # "Will the gods protect me?"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:487
translate chinesesim beijing_interview_after_214e2a5c:

    # "Thoughts invaded my mind as I looked at the shrine."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:488
translate chinesesim beijing_interview_after_1510d5df:

    # "Lúgōuqiáo Shìbiàn had begun only a few days ago."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:495
translate chinesesim beijing_interview_after_7409aa5f:

    # "Uncle Ku ambled out of the guest room with his hands concealed under his robes."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:496
translate chinesesim beijing_interview_after_303279c5:

    # "He grimaced slightly at the situation."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:497
translate chinesesim beijing_interview_after_1fd2c830:

    # Gu "You might have to wait to attain your visa."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:498
translate chinesesim beijing_interview_after_ae1f8bba:

    # "He seemed to say that calmly."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:499
translate chinesesim beijing_interview_after_7da38a44:

    # "Something so important...{w=1} taken so lightly."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:500
translate chinesesim beijing_interview_after_5f04ce66:

    # fang "That's fine...{w=1} I cant deal with that."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:501
translate chinesesim beijing_interview_after_ef2d5b62:

    # "Uncle Ku sat down at the shrine and opened Tao Te Ch'ing."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:502
translate chinesesim beijing_interview_after_cb9ff31f:

    # "He was a commited reader and devotee."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:503
translate chinesesim beijing_interview_after_e1d3b4ca:

    # Gu "You should go outside and find something to do."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:504
translate chinesesim beijing_interview_after_d69c0ab2:

    # "I looked back at Uncle Ku."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:505
translate chinesesim beijing_interview_after_21d1e615:

    # "He could only sigh."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:506
translate chinesesim beijing_interview_after_b244df2c:

    # "That was all he could do."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:507
translate chinesesim beijing_interview_after_097081e8:

    # Gu "{cps=*0.4}Don't worry{/cps}"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:508
translate chinesesim beijing_interview_after_f571e79d:

    # Gu "We can get ut of here in time."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:509
translate chinesesim beijing_interview_after_344d8dd3:

    # "Uncle Ku would reguarly meet in the village centre and talk to elders and intelectuals about current events."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:510
translate chinesesim beijing_interview_after_dffffef9:

    # Gu "Japan is mobilising an army, they won't go further than Peiping-Tientsen, {w=1}I've heard."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:511
translate chinesesim beijing_interview_after_b007fdc5:

    # "Uncle Ku felt hopeless."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:512
translate chinesesim beijing_interview_after_fd995bd2:

    # "Staring into those eyes felt like lies."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:513
translate chinesesim beijing_interview_after_6b84dc55:

    # fang "They keep taking from us."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:514
translate chinesesim beijing_interview_after_3dd3da8a:

    # Gu "I know..."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:515
translate chinesesim beijing_interview_after_1c30bec8:

    # "Uncle Ku didn't believe in violence,{w=1.0} he didn't feel the need to fight back."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:516
translate chinesesim beijing_interview_after_22dfc757:

    # fang "Now we have a drunk, a looming war {fast}and what more!" with sshake
    fang "" with sshake

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:517
translate chinesesim beijing_interview_after_562a9287:

    # "I had lost my composure."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:518
translate chinesesim beijing_interview_after_fb0f748a:

    # "I could see Uncle Ku's reaction to me lashing out after such a long time."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:519
translate chinesesim beijing_interview_after_3148a438:

    # "Pressure that had built up from everything had finally burst the bottle."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:520
translate chinesesim beijing_interview_after_ece2e030:

    # "Uncle Ku looked at me solenmly."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:521
translate chinesesim beijing_interview_after_3b10b877:

    # Gu "{cps=*0.4}I'm sorry,{w=1} Fang.{/cps}"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:522
translate chinesesim beijing_interview_after_6bc65d57:

    # fang "I'm sorry too."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:523
translate chinesesim beijing_interview_after_7ced2f82:

    # "I turned my back and walked out of the door."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:524
translate chinesesim beijing_interview_after_58d0b556:

    # Gu "If there is a air raid just run straight to some air raid shelter."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:525
translate chinesesim beijing_interview_after_cfdee768:

    # Gu "If you find a German one, priorotise that."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:526
translate chinesesim beijing_interview_after_70ecd2cf:

    # Gu "The German peoples don't get harassed by {i}guizi{/i}."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:531
translate chinesesim beijing_interview_after_987e54ed:

    # "I had crossed the Er-men."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:534
translate chinesesim beijing_interview_after_f62f03cc:

    # "I kept on walking until I passed through the Da-men."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:538
translate chinesesim beijing_interview_after_0e442468:

    # "Outside was a little garden where my Great Grandfather had planted a Sakura Tree for Sun Yat Sen's new era of the Republic."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:539
translate chinesesim beijing_interview_after_4c143651:

    # "It's a real shame I never even learnt his name despite knowing how well known he was in this local place."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:540
translate chinesesim beijing_interview_after_1e4a2fc5:

    # "My great grandfather disliked the Qing as they were Manchus as opposed to the Ming who were a Han Chinese dynasty."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:541
translate chinesesim beijing_interview_after_ef07f827:

    # "He had a wealthy wine business which gave him the money to buy a large {i}Siheyuan{/i}"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:542
translate chinesesim beijing_interview_after_89912be0:

    # "The Sakura was planted to mark the growth a new era without the Qing."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:543
translate chinesesim beijing_interview_after_83dd0953:

    # "The Sakura plant was also foreign and thus showcased his wealth when he had it planted."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:544
translate chinesesim beijing_interview_after_f8fbeab5:

    # "Everything I ever learnt about him indicated that...{w=1} wealth controlled his choices."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:545
translate chinesesim beijing_interview_after_10dfdf0d:

    # "Sadly he died before he realised how the Republic failed China."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:546
translate chinesesim beijing_interview_after_30c06d5a:

    # "{cps=*0.4}or the shortlived Empire before the warlord era.{/cps}"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:547
translate chinesesim beijing_interview_after_351853bb:

    # "Beyond the Sakura tree was a pond he had hired workers to build."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:548
translate chinesesim beijing_interview_after_fcab958a:

    # "Apart from showing off his wealth he would also use this pond to grow Lotuses."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:549
translate chinesesim beijing_interview_after_4a929fa0:

    # "He would grow them in bulks and sell them to Buddhists and Buddhist temples in which he would get blessed and make money."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:550
translate chinesesim beijing_interview_after_68c118a3:

    # "now..{w=1} he wasn't shallow that he did that for profit."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:551
translate chinesesim beijing_interview_after_af03b872:

    # "He was a devout buddhist."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:552
translate chinesesim beijing_interview_after_192a0b7c:

    # "He would donate large sums of charity to people in need and temples."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:553
translate chinesesim beijing_interview_after_c65c1e7d:

    # "I kept walking on until the pond came into view."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:554
translate chinesesim beijing_interview_after_92334364:

    # "Black and white pictures showed budding and blossomed Lotuses on the surface of the water."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:555
translate chinesesim beijing_interview_after_fd52d356:

    # "Earlier photos even had swans in them."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:556
translate chinesesim beijing_interview_after_7e1ebe42:

    # "What I saw in front of me was neither."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:557
translate chinesesim beijing_interview_after_e8a5682f:

    # "The water was brown, murky and thick."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:558
translate chinesesim beijing_interview_after_6db7e247:

    # "It was hard to believe anything nice was in there anymore."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:559
translate chinesesim beijing_interview_after_7629b577:

    # "Uncle Ku refused to spend his inheritance money on refurbishing things around the house."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:560
translate chinesesim beijing_interview_after_00862d6e:

    # "He dared not indulge in materialism and wealth of any kind."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:561
translate chinesesim beijing_interview_after_6fe907e6:

    # "He preferred and carried a metaphorical banner of being a rich man in poor clothing."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:562
translate chinesesim beijing_interview_after_ebe844e9:

    # "I stared at the pathetic pond."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:563
translate chinesesim beijing_interview_after_d0f41e1e:

    # "The pond that gave up on life."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:564
translate chinesesim beijing_interview_after_5519c874:

    # "Gave up on struggling."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:565
translate chinesesim beijing_interview_after_700aaef1:

    # Lc "He should have hired people to dig a well." with sshake
    Lc "" with sshake

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:566
translate chinesesim beijing_interview_after_53f4819f:

    # "I turned around fast before facing Lao-Chang."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:567
translate chinesesim beijing_interview_after_ef41dda5:

    # "We called him that because he was an old milkman who didn't want to retire.{nw}"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:568
translate chinesesim beijing_interview_after_766758c2:

    # "Hence the name \"old Chang\"."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:569
translate chinesesim beijing_interview_after_954620d2:

    # fang "I would agree."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:570
translate chinesesim beijing_interview_after_47c7117c:

    # fang "That would have been better for the community."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:571
translate chinesesim beijing_interview_after_51cf1c6b:

    # Lc "We have a groundwater well over on the other side that was dug by Ma Wen's father."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:572
translate chinesesim beijing_interview_after_40f7e397:

    # Lc "It's near the animals a lot, people are scared that they'll find chicken shit."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:573
translate chinesesim beijing_interview_after_a9e99285:

    # fang "Uncle Ku is a miser wearing the mask of Taoism."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:574
translate chinesesim beijing_interview_after_32905f9c:

    # Lc "That's your opinion."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:575
translate chinesesim beijing_interview_after_ff0ca3f8:

    # Lc "I'm sure if we held a petition he would support it."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:576
translate chinesesim beijing_interview_after_99f1e7bb:

    # fang "Really?"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:577
translate chinesesim beijing_interview_after_166f331d:

    # "I wasn't sure if that would suspend my disbelief."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:578
translate chinesesim beijing_interview_after_e3fa8c6c:

    # Lc "Calling him a miser won't do him justice."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:579
translate chinesesim beijing_interview_after_58c4b395:

    # Lc "He isn't the kind of person who spends money on themselves."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:580
translate chinesesim beijing_interview_after_c914987d:

    # Lc "He would rather spend it to support others."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:581
translate chinesesim beijing_interview_after_4a0c57eb:

    # "He was old enough to know how everyone functioned in this community."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:582
translate chinesesim beijing_interview_after_0592966d:

    # fang "If you ask him, he will."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:583
translate chinesesim beijing_interview_after_6eac22d6:

    # "Lao Chang laughed."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:584
translate chinesesim beijing_interview_after_b5e3e99c:

    # Lc "You remind me of your grandpa."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:585
translate chinesesim beijing_interview_after_6497d090:

    # "Lao Chang sat down on the damp grass next to me that faced the miserable pond."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:586
translate chinesesim beijing_interview_after_3480fcf4:

    # fang "How so?"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:587
translate chinesesim beijing_interview_after_34567f3c:

    # Lc "Whenever there was war...{w=1} the fella just got into a bad mood."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:588
translate chinesesim beijing_interview_after_660b721e:

    # Lc "During the northern expedition when Chiang Chieh-shih tried to take back from those warlords.."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:589
translate chinesesim beijing_interview_after_bcba409c:

    # Lc "He had a sulky mood for a week."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:590
translate chinesesim beijing_interview_after_10eb640b:

    # "Lao Chang dug his hand into the basket tied to his bicycle and took out a rice snack."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:591
translate chinesesim beijing_interview_after_4fd10ba3:

    # Lc "You see. {w=1}His sister was back in Kwangchow where this crazy reunifier was wailing war and unifying cities under his control."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:592
translate chinesesim beijing_interview_after_febdeee5:

    # Lc "He wouldn't get any of her letters from kwangchow so he sulked."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:593
translate chinesesim beijing_interview_after_de51510d:

    # "Lao Chang bit into his rice snack and chewed it open mouthed."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:594
translate chinesesim beijing_interview_after_7de84871:

    # Lc "He sent out a lot of letters to."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:595
translate chinesesim beijing_interview_after_b515e6b4:

    # Lc "I wonder where they went."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:596
translate chinesesim beijing_interview_after_cd9e96e6:

    # "Lao Chang gazed at the shitty pond. Like it was beautiful as my Great-Grandfather had left it."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:597
translate chinesesim beijing_interview_after_b06eb32e:

    # Lc "Eventually you got this shithole situation where there are so many warlords and everyone wants to crown a new China."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:598
translate chinesesim beijing_interview_after_6c67dca3:

    # Lc "Who do you think will crown a new China?"
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:601
translate chinesesim beijing_interview_after_45f1cd81:

    # fang "I think Chiang Chie Shih"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:602
translate chinesesim beijing_interview_after_56e5583c:

    # fang "He has proclaimed central government."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:603
translate chinesesim beijing_interview_after_c8424e1e:

    # fang "The warlords even cooperate with him."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:604
translate chinesesim beijing_interview_after_5ca73828:

    # fang "It's more or less if he can centralize power to one form of government."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:605
translate chinesesim beijing_interview_after_37ba2c0e:

    # Lc "That's a good bet."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:606
translate chinesesim beijing_interview_after_3dab7ea6:

    # Lc "The national Revolutionary Army rolled over everyone in the Northern Camapaign."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:607
translate chinesesim beijing_interview_after_84c21822:

    # Lc "If they keep those troops moralised Dr Sun's democracy will come back to us."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:609
translate chinesesim beijing_interview_after_5dc869d2:

    # fang "Chiang Chi Shih seems to be too right wing and authoritarian."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:610
translate chinesesim beijing_interview_after_2df348af:

    # fang "He doesn't cooperate with appeasing the Japanese or the warlords."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:611
translate chinesesim beijing_interview_after_d05bc75d:

    # fang "Personally Wang Ching Wei's left Kuomintang will be more liked by foreign nations."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:612
translate chinesesim beijing_interview_after_29b315e1:

    # Lc "The left Kuomintang are an interesting bunch."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:613
translate chinesesim beijing_interview_after_b3c8a0dc:

    # Lc "I can't read so I never get much info about politics."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:614
translate chinesesim beijing_interview_after_8eae81eb:

    # fang "Wang Ching Wei was close with Dr Sun in the first republic."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:615
translate chinesesim beijing_interview_after_1e64a525:

    # fang "He lost the power struggle in the Kuomintang to Chiang Chie Shih."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:616
translate chinesesim beijing_interview_after_9aae0b63:

    # fang "I think he thinks well."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:617
translate chinesesim beijing_interview_after_f7238d44:

    # Lc "I think he's a bit too crazy for the Guizi."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:618
translate chinesesim beijing_interview_after_57dacea0:

    # Lc "He prefers to appease a rising colonial power."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:619
translate chinesesim beijing_interview_after_37b45375:

    # Lc "That's basically feeding a pig a little snack so it won't eat a lot."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:620
translate chinesesim beijing_interview_after_a140f562:

    # fang "I guess, I understand what you mean."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:621
translate chinesesim beijing_interview_after_afd0d03b:

    # Lc "Plus Wang doesn't ever cooperate with Chiang, I think Wang will leave and form his own little thing like he did back in Wuhan."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:622
translate chinesesim beijing_interview_after_9d0b78d9:

    # fang "I think so too."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:623
translate chinesesim beijing_interview_after_d8a385ec:

    # Lc "Ever since he failed with cooperating with the Yan'an communists, he's becoming more and more right wing."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:624
translate chinesesim beijing_interview_after_de6e47dd:

    # Lc "Ever since the republic was established, the biggest worry is extremism."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:625
translate chinesesim beijing_interview_after_c11b339f:

    # "Lao Chang took a bit out of his rice snack."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:626
translate chinesesim beijing_interview_after_47c18bb2:

    # Lc "Everyone is thinking \"What form of government is fit to rule 460 million people?\"."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:627
translate chinesesim beijing_interview_after_50dd11be:

    # Lc "Then they all start shooting each other."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:628
translate chinesesim beijing_interview_after_41874980:

    # "Lao Chang gives a hearty laugh at this satirical joke he made."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:629
translate chinesesim beijing_interview_after_a0e7c27c:

    # "I grin in response."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:630
translate chinesesim beijing_interview_after_5d4d4db0:

    # fang "but I think we should at least appease until our military is ready to right Japan."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:631
translate chinesesim beijing_interview_after_ec376d98:

    # fang "I don't know if full scale war will happen from the Marco-Polo bridge incident..{w=1}but it is imminent.{nw}"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:632
translate chinesesim beijing_interview_after_0c37e037:

    # fang "That's for sure."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:633
translate chinesesim beijing_interview_after_0be7aa00:

    # Lc "I guess, it's depressing we got to live through this."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:635
translate chinesesim beijing_interview_after_d2a03b79:

    # fang "I think Li Tsung-Jen"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:636
translate chinesesim beijing_interview_after_fcd2fb82:

    # Lc "The Kwanghsi leader?"
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:637
translate chinesesim beijing_interview_after_b2278578:

    # fang "He and Pai Ch'ung-hsi were the ones that captured Beijing."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:638
translate chinesesim beijing_interview_after_d7405b5c:

    # fang "I think they have quite an influence over the central government."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:639
translate chinesesim beijing_interview_after_e680bd94:

    # fang "I wouldn't be suprised if Li Tsung-Jen was reinstated as a general and rose up the ranks in Chiang's government."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:640
translate chinesesim beijing_interview_after_599d47f5:

    # Lc "He is quite capable."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:641
translate chinesesim beijing_interview_after_701384f1:

    # Lc "I like how he works."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:643
translate chinesesim beijing_interview_after_c47c6583:

    # fang "The communists were supposed to be kicked out of Yan'an."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:644
translate chinesesim beijing_interview_after_efcdc3cf:

    # fang "After Chang Hsueh-liang took Chiang hostage to focus on the Japanese they stopped attacking the communists."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:645
translate chinesesim beijing_interview_after_4577d62d:

    # fang "Now that they are thriving and gaining support I think they have a chance after the war to seize victory against an unstabke governmnet."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:646
translate chinesesim beijing_interview_after_9b798a35:

    # fang "I'm no communist, but I think when all this ends, Mao's face will be on the Tiananmen Square."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:647
translate chinesesim beijing_interview_after_543f5afb:

    # Lc "His rhetoric is good also."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:648
translate chinesesim beijing_interview_after_3a110016:

    # "Lao Chang pulled out some grass as he munched on another bite of his rice snack."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:649
translate chinesesim beijing_interview_after_be204bd2:

    # Lc "Those poor peasants want land, freedom and empowerment."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:650
translate chinesesim beijing_interview_after_f9fcbc56:

    # Lc "The communist revolution gives them that oppurtunity."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:651
translate chinesesim beijing_interview_after_a22676c8:

    # fang "How do you know so much about the communists?"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:652
translate chinesesim beijing_interview_after_de711df8:

    # "Lao Chang munched on his snack and turned to face me."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:653
translate chinesesim beijing_interview_after_3eec8855:

    # Lc "The village center always tells what they're up to so he can undermine them and their anti-buddhist, anti-taoist philosophies."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:654
translate chinesesim beijing_interview_after_998bc053:

    # Lc "It looks like foolish advertising to me."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:655
translate chinesesim beijing_interview_after_a1704a67:

    # "Lao Chang took another bite from his rice snack."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:656
translate chinesesim beijing_interview_after_b1e40fc1:

    # Lc "I think I'm happy giving milk to people everyday."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:657
translate chinesesim beijing_interview_after_afe500a8:

    # Lc "My ribs might be showing a lot, but my good deeds will grant me something good in my next life."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:658
translate chinesesim beijing_interview_after_96305c83:

    # Lc "Even if there isn't a next life at least I'll be happy in this one."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:659
translate chinesesim beijing_interview_after_68f6df2f:

    # Lc "Who knows maybe my hard work will pay off and I'll live in a palace of gold even."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:660
translate chinesesim beijing_interview_after_c38957b4:

    # "Lao Chang gave a dry hearty laugh."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:662
translate chinesesim beijing_interview_after_d9b0b5c9:

    # fang "I don't know."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:663
translate chinesesim beijing_interview_after_22dd67ae:

    # fang "Normally I don't engage in politics so much."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:664
translate chinesesim beijing_interview_after_36206a54:

    # Lc "Eh?"
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:665
translate chinesesim beijing_interview_after_76db1228:

    # Lc "That's fine."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:666
translate chinesesim beijing_interview_after_40bd1dc6:

    # Lc "I don't have an interest."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:667
translate chinesesim beijing_interview_after_9834a8ea:

    # Lc "If I know anything about politics it's because Guo Heng's father always shouts about it in the village center over some tea."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:668
translate chinesesim beijing_interview_after_59082eef:

    # Lc "He's an avid historian who sits at home all day like your Uncle and does scholary stuff."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:669
translate chinesesim beijing_interview_after_f5fec118:

    # "Lao Chang takes a bit from rice snack."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:670
translate chinesesim beijing_interview_after_f9a42f4d:

    # Lc "but I get you. Don't stick your head into that stuff too much."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:671
translate chinesesim beijing_interview_after_1ddee56a:

    # "Lao Chang stood up and patted off the back of his pants that had attracted clumps of damp soil."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:672
translate chinesesim beijing_interview_after_47748fef:

    # Lc "I gotta get going."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:673
translate chinesesim beijing_interview_after_d00f25a1:

    # Lc "Wanna buy a bottle of milk?"
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:674
translate chinesesim beijing_interview_after_e03cf285:

    # Lc "You can take it home early."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:675
translate chinesesim beijing_interview_after_1b175b3d:

    # fang "No thanks, I don't have cash."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:676
translate chinesesim beijing_interview_after_a67fad66:

    # fang "Uncle Ku will pay for it at the doorstep."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:677
translate chinesesim beijing_interview_after_be51f2c4:

    # Lc "I guess that suits it."
    Lc ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:678
translate chinesesim beijing_interview_after_82ee94b0:

    # "Lao Chang grabbed the handles of his makeshift bike and pushed it around."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:679
translate chinesesim beijing_interview_after_36a06012:

    # "THe odd thing about Lao Chang was that he would have a bike he would never ride."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:680
translate chinesesim beijing_interview_after_4eaa36b5:

    # "He would just use the basket and push it for ease."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:681
translate chinesesim beijing_interview_after_43007ff0:

    # "Since he was twice the age of Uncle Ku it made sense."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:682
translate chinesesim beijing_interview_after_e5466bb9:

    # "The bicycle was also too small for him. {w=1}I would have assumed that this was something thrown away by the child of some wealthy family who grew out of the bike."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:683
translate chinesesim beijing_interview_after_5c261c14:

    # "I observed Lao Chang walk off in the distance, pushing his bike and the flicking the rusted bell that wouldn't make any sound. {w=1}That was because there was nothing for it to strike against."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:684
translate chinesesim beijing_interview_after_94720b3e:

    # "I turned and began to walk the other way."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:685
translate chinesesim beijing_interview_after_63763b69:

    # "It was a five minute walk away from the village center."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:686
translate chinesesim beijing_interview_after_c8944ef8:

    # "Most young boys or able men would not be welcome there."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:687
translate chinesesim beijing_interview_after_2cad8397:

    # "This was because young or able men would get kidnapped by the Kuomintang army and chained and walked down to military headquarters to forcefully enlist."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:688
translate chinesesim beijing_interview_after_b99bb321:

    # "These days the reports of Kuomintang barging and taking had gone down."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:689
translate chinesesim beijing_interview_after_58fdec37:

    # "My shoes rubbed in the dirt pass as I ambled on."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:690
translate chinesesim beijing_interview_after_b6c56af5:

    # "In six months of residing in this district I had a memorised map of the place."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:691
translate chinesesim beijing_interview_after_8be38699:

    # "There were a few facilities I was going to walk past."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:692
translate chinesesim beijing_interview_after_396ad81c:

    # "The most interesting was the brothel."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:693
translate chinesesim beijing_interview_after_ede26bbe:

    # "The sex industry had grown in Manchuria and many boys would talk about it in the alley ways."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:694
translate chinesesim beijing_interview_after_8b1f9ec4:

    # "Over here the brothel was something that you should not be seen entering and leaving if you wanted to have a sense of dignity."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:695
translate chinesesim beijing_interview_after_2d81a280:

    # "This brothel was known as a {i}Chang San{/i} brothel."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:696
translate chinesesim beijing_interview_after_c758d237:

    # "It was like an artistic form of prostituition in which the prositute would play the lyre, do calligraphy and do paintings."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:697
translate chinesesim beijing_interview_after_73568195:

    # "I think it was a progressive way to humanise prostitutes."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:698
translate chinesesim beijing_interview_after_15762c58:

    # "Personally I have never interacted with a prosititute."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:699
translate chinesesim beijing_interview_after_0bec31ee:

    # "Nor do I have a good enough reason to."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:700
translate chinesesim beijing_interview_after_7422fa1e:

    # "I just don't understand their stigma in society."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:701
translate chinesesim beijing_interview_after_e542c12c:

    # "Many of the girls are of varying ages from 13 to early 40s."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:702
translate chinesesim beijing_interview_after_d509dd14:

    # "The place was not regulated much with ramapnt bribing and blackmail."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:703
translate chinesesim beijing_interview_after_1702b1dc:

    # prostitute "Hey Fang!"
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:704
translate chinesesim beijing_interview_after_eb499bee:

    # "I turned to see her sitting in a chair outside the door."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:705
translate chinesesim beijing_interview_after_43dfb42d:

    # fang "Hey"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:706
translate chinesesim beijing_interview_after_ad1e0190:

    # "I did a faint wave."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:707
translate chinesesim beijing_interview_after_07c63af0:

    # prostitute "Is that a wave?"
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:708
translate chinesesim beijing_interview_after_9b893eff:

    # prostitute "Relax, {w=1}waving back to me won't make me pounce on you."
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:709
translate chinesesim beijing_interview_after_b9e3ded9:

    # "She crossed her arms and smiled at me."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:710
translate chinesesim beijing_interview_after_550ec0c4:

    # prostitute "I have my principles."
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:711
translate chinesesim beijing_interview_after_f216f565:

    # "She grinned."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:712
translate chinesesim beijing_interview_after_837b248e:

    # "I didn't know how to respond to her."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:713
translate chinesesim beijing_interview_after_785bf038:

    # "All I could give her was an awkward grin."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:714
translate chinesesim beijing_interview_after_ad3a7259:

    # "She giggled at my response."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:715
translate chinesesim beijing_interview_after_a2afebb3:

    # prostitute "Hard to believe{cps=*0.4}...{/cps}{w=1}Some {i}whore{/i} has a sense of principle. Right?"
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:716
translate chinesesim beijing_interview_after_1ebedaf1:

    # "I looked at the ground."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:717
translate chinesesim beijing_interview_after_c5676ad7:

    # "At this point I had trouble making any form of eye contact."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:718
translate chinesesim beijing_interview_after_71b9caa3:

    # prostitute "Don't feel bad, little brother."
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:719
translate chinesesim beijing_interview_after_469031b8:

    # prostitute "I mean, my rules are I don't drink,{w=1} I don't smoke and I don't condone violence."
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:720
translate chinesesim beijing_interview_after_f3d21a0f:

    # "She leaned back on her chair against the chipped brick blocks."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:721
translate chinesesim beijing_interview_after_7afb09f0:

    # prostitute "I have a band of sisters to look after. People in my situation."
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:722
translate chinesesim beijing_interview_after_2154be15:

    # "I couldn't keep being judged like this."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:723
translate chinesesim beijing_interview_after_373af892:

    # "I felt a need to spit it out."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:724
translate chinesesim beijing_interview_after_91aa3393:

    # fang "I don't see whatever you're doing as a bad thing."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:725
translate chinesesim beijing_interview_after_52a4bb62:

    # "I interrupted her mid sentence."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:726
translate chinesesim beijing_interview_after_915b5bbf:

    # "She gave a strange look."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:727
translate chinesesim beijing_interview_after_b0518a31:

    # prostitute "Litte brother, if some alleyboy said those words to me, I'd think it's some ploy for pity lovemaking."
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:728
translate chinesesim beijing_interview_after_29fd12b7:

    # prostitute "I know my stigma,{cps=*1.2} don't pity me..{/cps}"
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:729
translate chinesesim beijing_interview_after_93c9e6ba:

    # "She was still in a good mood for some odd reason."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:730
translate chinesesim beijing_interview_after_bd1a3dbe:

    # prostitute "You know, the conflict happening up there doesn't bother me."
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:731
translate chinesesim beijing_interview_after_68455a7a:

    # prostitute "If the Japanese do take over, we will just be required to service their soldiers instead."
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:732
translate chinesesim beijing_interview_after_241eb0c0:

    # "She closed her eyes to laugh at herself."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:733
translate chinesesim beijing_interview_after_4774f5ae:

    # prostitute "I mean the industry is booming in Dong-bei, so things around here will change I guess."
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:734
translate chinesesim beijing_interview_after_897f42cf:

    # fang "Alley boys told you right?"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:735
translate chinesesim beijing_interview_after_66e0338a:

    # prostitute "I mean who else will?"
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:736
translate chinesesim beijing_interview_after_f56a765a:

    # prostitute "it's nice talking to a non-pervert for once."
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:737
translate chinesesim beijing_interview_after_6ea96360:

    # prostitute "It's amazing how people can have fruitful conversations if you give them a chance. Right Fang?"
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:738
translate chinesesim beijing_interview_after_88734666:

    # fang "Right."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:739
translate chinesesim beijing_interview_after_8c248d20:

    # "How did she know my name?"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:740
translate chinesesim beijing_interview_after_cf190f9e:

    # fang "How do you know my name?"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:741
translate chinesesim beijing_interview_after_03c6d4d8:

    # prostitute "The other girls you flirt with when they sit outside told me so."
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:742
translate chinesesim beijing_interview_after_018fee3c:

    # "I felt my face go red."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:743
translate chinesesim beijing_interview_after_3a5020f0:

    # prostitute "You talk better to the ones your age."
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:744
translate chinesesim beijing_interview_after_aeae8897:

    # prostitute "I'm only a few years your senior. {w=1}A solid 19 year old."
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:745
translate chinesesim beijing_interview_after_dfc3ff50:

    # prostitute "You're disappointed. Aren't you?"
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:746
translate chinesesim beijing_interview_after_1320766d:

    # "She was teasing me."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:747
translate chinesesim beijing_interview_after_e2317379:

    # prostitute "The ones you talk to aren't here."
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:748
translate chinesesim beijing_interview_after_4e847e22:

    # "It seemed their sisterhood all shared the same mind like some collective conscious."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:749
translate chinesesim beijing_interview_after_837753ac:

    # prostitute "They tell me, how you blush when they compliment you."
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:750
translate chinesesim beijing_interview_after_cf571af2:

    # prostitute "Maybe when you're older you can whisk one away and keep her."
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:751
translate chinesesim beijing_interview_after_0aae71c1:

    # "I gave a faint giggle as I felt butterflies in my stomach."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:752
translate chinesesim beijing_interview_after_40b09c5d:

    # prostitute "Xiao Wen dreams about you, I've heard."
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:753
translate chinesesim beijing_interview_after_a62072d5:

    # "That took me off guard."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:754
translate chinesesim beijing_interview_after_3cd10ac6:

    # "I could feel my heart pumping and racing."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:755
translate chinesesim beijing_interview_after_0ea2079e:

    # "It was just embarassing at this point."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:756
translate chinesesim beijing_interview_after_b3a20742:

    # prostitute "I think you two would be cute."
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:757
translate chinesesim beijing_interview_after_9570245f:

    # prostitute "By the way, my name is Wang Yuelan."
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:758
translate chinesesim beijing_interview_after_a6d8cd0b:

    # prostitute "If you ever need shelter, just ask Xiao Wen to share the room."
    prostitute ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:759
translate chinesesim beijing_interview_after_163693db:

    # "She laughed as she opened the door and walked inside."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:760
translate chinesesim beijing_interview_after_642efc41:

    # "They were an interesting bunch,{w=1}strange but interesting."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:761
translate chinesesim beijing_interview_after_783979dc:

    # "While I do confess Xiao Wen's attraction, caste was a stigma that would burrow itself deep in me, like some toxic idea or opinion."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:762
translate chinesesim beijing_interview_after_1d3c70c3:

    # "I felt guilty to admit to myself such things."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:764
translate chinesesim beijing_interview_after_6ae16ae0:

    # "Despite that..."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:765
translate chinesesim beijing_interview_after_65a8d782:

    # "I couldn't help wanting to... {w=1} touch her."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:766
translate chinesesim beijing_interview_after_dd633f65:

    # "Like feel her skin,"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:767
translate chinesesim beijing_interview_after_6b9ee190:

    # "Hear her moan."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:768
translate chinesesim beijing_interview_after_715adfaf:

    # "It would keep me up all night until I did something about it."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:769
translate chinesesim beijing_interview_after_4ef95325:

    # "At this point I didn't know when I felt lust for her,{w=2} and when I felt love."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:770
translate chinesesim beijing_interview_after_a881e8d8:

    # "She would flirt with me everytime I would pass by and no one else was around."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:771
translate chinesesim beijing_interview_after_831ae74b:

    # "It was intense."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:772
translate chinesesim beijing_interview_after_970bf175:

    # "Despite that..{w=1}I can't keep thinking so provocatively."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:773
translate chinesesim beijing_interview_after_6c093dc8:

    # "I had to come to my senses."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:774
translate chinesesim beijing_interview_after_62d34048:

    # "I kept on walking along the dirt path."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:775
translate chinesesim beijing_interview_after_07abb7c3:

    # "The ambient noises from the brothel faded away with my distance."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:776
translate chinesesim beijing_interview_after_e26f0483:

    # "I kept walking on."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:777
translate chinesesim beijing_interview_after_cd9f35e2:

    # "Each step one by one."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:778
translate chinesesim beijing_interview_after_b37e1bb6:

    # "The next facility I would see would be the winery my Great Grandpa owned."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:779
translate chinesesim beijing_interview_after_8c489c8b:

    # "Uncle Ku had sold it when he inherited it from his father."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:780
translate chinesesim beijing_interview_after_22294c9d:

    # "Uncle Ku didn't want to support himself by destroying the lives of addicts."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:781
translate chinesesim beijing_interview_after_aad5fabc:

    # "At first he kept it out of operation."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:782
translate chinesesim beijing_interview_after_4c9d7840:

    # "It would sit there 5-6 years...{w=1}undisturbed."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:783
translate chinesesim beijing_interview_after_347511fe:

    # "Then officials would come and start ordering him to do something productive with that land and facilities."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:784
translate chinesesim beijing_interview_after_fc494a88:

    # "After a few months he sold it off to some businessman from Shanghai who wished to expand their winery business."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:785
translate chinesesim beijing_interview_after_20aede96:

    # "Now I walk past it and just look at how many cycles this building has experienced."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:786
translate chinesesim beijing_interview_after_7cddeb84:

    # "I was never told why this building was built intially, since my Great Grandfather just decided to occupy this empty place for good business."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:787
translate chinesesim beijing_interview_after_f37e5464:

    # "His father ran a little business from his home.{w=1}So my Great Grandpa decided what to do,"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:788
translate chinesesim beijing_interview_after_2f125b80:

    # "He moved all his stuff there and took care of it."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:789
translate chinesesim beijing_interview_after_b26e26af:

    # "Eventually people began to think this abandoned building didn't host any curse or haunted by any angry spirits."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:790
translate chinesesim beijing_interview_after_da09e6f2:

    # "Some wealthy people like the Ma family invested in the winery and it grew bigger."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:791
translate chinesesim beijing_interview_after_f983088a:

    # "In about 3 years the bulding had been expanded to four times its intial size."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:792
translate chinesesim beijing_interview_after_abec27e1:

    # "Over 70 people worked here for good pay."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:793
translate chinesesim beijing_interview_after_208666c8:

    # "My Great Grandpa built an expensive Siheyuan and began to live lavishly like the Ma family."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:794
translate chinesesim beijing_interview_after_aa05e8a3:

    # "He died believing resourcefulness and willpower outperforms fate."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:795
translate chinesesim beijing_interview_after_911aeb79:

    # "I would get told all about this from the locals like Lao Chang or Ah-Mei the educated daughter of a noble family residing here."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:796
translate chinesesim beijing_interview_after_ab75596d:

    # "Eventually passing the collosal winery I began to see the village centre in sight."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:797
translate chinesesim beijing_interview_after_bc99f5af:

    # "It wasn't a centre where you would assume it was a meeting spot."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:798
translate chinesesim beijing_interview_after_8e0a4fef:

    # "It was a noodle shop run by the Guo family."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:799
translate chinesesim beijing_interview_after_c50e6e61:

    # "Everyone would come and eat there once in a while and that became the de-facto village centre."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:800
translate chinesesim beijing_interview_after_565216ae:

    # "It was run by two brothers around my age."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:801
translate chinesesim beijing_interview_after_dfc609f0:

    # "The older was Guo Heng, who took over the shop when his father passed away from a stroke two years ago."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:802
translate chinesesim beijing_interview_after_92584921:

    # "He raised his younger brother to learn to cook as well."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:803
translate chinesesim beijing_interview_after_1be4a2aa:

    # "Eventually they got their skills developed and Guo Heng got to send his brother \"Guo He\" to school."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:804
translate chinesesim beijing_interview_after_a1b157f2:

    # "I have met Guo Heng only a few times,{w=0.5} but he is very interesting to talk to."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:805
translate chinesesim beijing_interview_after_79d600b7:

    # "He has developed a passion for making all kinds of noodles."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:806
translate chinesesim beijing_interview_after_7b0867f8:

    # "Last time I visited him he showed me his father's recipe that had been handed down for god knows how long."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:807
translate chinesesim beijing_interview_after_6a3d1e25:

    # "He offered to employ me to his noodle shop if I ever needed a place to work."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:808
translate chinesesim beijing_interview_after_faefdff6:

    # "Despite being 23 years old Guo Heng was arguably graying from his stress.{w=1} Something he always loved joking about with the other villagers."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:809
translate chinesesim beijing_interview_after_535c279e:

    # "The noodle shop even had a banner which would ostentaciously show off its wealth and success."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:810
translate chinesesim beijing_interview_after_8426b03c:

    # "In short, if there was any celebration, meeting, or festival, Guo's noodle shop was the place."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:811
translate chinesesim beijing_interview_after_326d4b39:

    # "Mostly because it was the most central and most convenint for everyone to meet at."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:812
translate chinesesim beijing_interview_after_e67642e6:

    # "I passed through and entered the store."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:813
translate chinesesim beijing_interview_after_ce064a8e:

    # "Guo Heng had sat on a table and was appying a dirty rag to wipe the sweat off his face."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:814
translate chinesesim beijing_interview_after_86d5155b:

    # "The summer sun was strong and it didn't help he cooked behind a fire all day."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:815
translate chinesesim beijing_interview_after_8f0c3adc:

    # "Guo Heng seemed to age faster everytime I visited him."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:816
translate chinesesim beijing_interview_after_2cc75be1:

    # "His body had become more built and skinny while his face became browner with more wrinkles."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:817
translate chinesesim beijing_interview_after_15409968:

    # "His hair had remained the same, {w=1}Little whisks of grey popping out once in an occassional spot."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:818
translate chinesesim beijing_interview_after_fb98cea9:

    # "Guo Heng looked up at me with his brown eyes that resembled foreign manufactured chocolate."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:819
translate chinesesim beijing_interview_after_ff3f5827:

    # Gh "Fang!{w=1}{nw}"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:820
translate chinesesim beijing_interview_after_33038952:

    # "I sat down opposite on the same table he was sitting at."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:821
translate chinesesim beijing_interview_after_f9e77a3e:

    # fang "Hey, Heng"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:822
translate chinesesim beijing_interview_after_c3affa23:

    # "It was midday and the sun was now forcing its heated will on us like spears."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:824
translate chinesesim beijing_interview_after_d9f51e51:

    # "At the highest point in the sky, a false inferno had formed."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:825
translate chinesesim beijing_interview_after_8678494b:

    # fang "You must hate the sun so much if you go through this everyday."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:826
translate chinesesim beijing_interview_after_e5df7b26:

    # "Guo Heng gave a smirk and dabbed his white dirtied rag on his face."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:827
translate chinesesim beijing_interview_after_379a5857:

    # "His shirt had wet patches near his armpits and chest."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:828
translate chinesesim beijing_interview_after_2f87023d:

    # Gh "I think its fair."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:829
translate chinesesim beijing_interview_after_3c2111fc:

    # fang "How is this fair?"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:830
translate chinesesim beijing_interview_after_e1d68c26:

    # "Guo Heng was quite wise because the elders would gather here and include him in philosophical talks."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:831
translate chinesesim beijing_interview_after_a4a6f097:

    # Gh "There are two things in this world which are fair."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:832
translate chinesesim beijing_interview_after_e34340ff:

    # Gh "THese two things never judge...{w=1}never discriminate."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:833
translate chinesesim beijing_interview_after_92e6f727:

    # "He dabbed the cloth on his face again."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:834
translate chinesesim beijing_interview_after_a3e8a284:

    # Gh "The first is death. In this world God and the Devil may judge you, but death will come for you, no matter how good,{w=1}how bad, or whatever you are."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:835
translate chinesesim beijing_interview_after_0fe09722:

    # Gh "Death doesn't pick favourites."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:836
translate chinesesim beijing_interview_after_c7eb74ff:

    # Gh "The Second is the sun."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:837
translate chinesesim beijing_interview_after_2ee779ee:

    # Gh "For Aeons the Sun had given light regardless of the morality of the creature receiving it."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:838
translate chinesesim beijing_interview_after_d3c7711c:

    # Gh "The sun when it's too hot will feel this way to everyone."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:839
translate chinesesim beijing_interview_after_c1516ef9:

    # Gh "If the sun isn't judging than whatever the sun does,{w=1}is fair."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:840
translate chinesesim beijing_interview_after_89dececc:

    # fang "You really learn some interesting stuff from those elders."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:841
translate chinesesim beijing_interview_after_040a32af:

    # Gh "I know right."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:842
translate chinesesim beijing_interview_after_83ff575a:

    # Gh "They don't have anything to do, so i give them discounted noodles and educate myself."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:843
translate chinesesim beijing_interview_after_ecd7e47a:

    # "I looked up at the sky"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:844
translate chinesesim beijing_interview_after_a4b2f697:

    # "It was clear and cloudless."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:845
translate chinesesim beijing_interview_after_78daf9f8:

    # fang "Don't you get worried about the Japanese?"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:846
translate chinesesim beijing_interview_after_022e8fed:

    # "Heng looked up to the sky."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:847
translate chinesesim beijing_interview_after_56edb4c3:

    # Gh "I heard they're readying air raid shelters because the Japanese are mobilsing for a full attack."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:848
translate chinesesim beijing_interview_after_8db74e93:

    # Gh "Maybe I'll have to learn to cook Ramen."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:849
translate chinesesim beijing_interview_after_7e555d0b:

    # fang "What about your younger brother?"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:850
translate chinesesim beijing_interview_after_052d306e:

    # Gh "I'm planning to send him to Nanking where he will be safer."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:851
translate chinesesim beijing_interview_after_6a13d02e:

    # "Heng dabbed the cloth on his face again."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:852
translate chinesesim beijing_interview_after_784ff6c1:

    # "His hair had beads of sweat bouncing but never dropping off."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:853
translate chinesesim beijing_interview_after_99f1e7bb_1:

    # fang "Really?"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:854
translate chinesesim beijing_interview_after_bac7774f:

    # Gh "It's a good idea to evacuate before they start fighting."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:855
translate chinesesim beijing_interview_after_b119e0f6:

    # Gh "Ma Wen told me that the Shanghai International Zone would become a passage of entrance for the Japanese."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:856
translate chinesesim beijing_interview_after_84ae348e:

    # Gh "He advised me not to send Guo He to Shanghai."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:857
translate chinesesim beijing_interview_after_93bd3d79:

    # Gh "Hence I chose Nanking."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:858
translate chinesesim beijing_interview_after_bf5fffc2:

    # Gh "The Japanese would probably confiscate the shop and make me do work without profit."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:859
translate chinesesim beijing_interview_after_4cbd20df:

    # fang "That is true."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:860
translate chinesesim beijing_interview_after_9b376429:

    # fang "I'm from [hometown!t]"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:862
translate chinesesim beijing_interview_after_fe6550d7:

    # fang "I can't go back to Taiwan."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:863
translate chinesesim beijing_interview_after_3f9a3683:

    # fang "They won't believe me since I'm crossing over from the Mainland."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:864
translate chinesesim beijing_interview_after_bb053616:

    # fang "I won't see my family till the end of the war."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:865
translate chinesesim beijing_interview_after_b222d438:

    # Gh "I get your pain."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:866
translate chinesesim beijing_interview_after_f3479dc2:

    # Gh "I think you should go to Nanking."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:867
translate chinesesim beijing_interview_after_2a98fdbc:

    # Gh "THat way Guo He is safe with you."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:868
translate chinesesim beijing_interview_after_576c1500:

    # fang "You really think so?"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:869
translate chinesesim beijing_interview_after_ef0e8cb7:

    # "Guo Heng gave a slight nod."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:870
translate chinesesim beijing_interview_after_817cbb00:

    # "He was willowed and his left leg had to limp."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:871
translate chinesesim beijing_interview_after_a2160b63:

    # "I could see why he couldn't leave as easily as he said it."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:872
translate chinesesim beijing_interview_after_5e283727:

    # Gh "So why did you leave Taiwan in te first place?"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:873
translate chinesesim beijing_interview_after_4445f46d:

    # fang "because I was able to."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:874
translate chinesesim beijing_interview_after_7ddd77b5:

    # fang "I just wanted to keep in touch with my history."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:875
translate chinesesim beijing_interview_after_693cb46f:

    # "Guo Heng nodded."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:876
translate chinesesim beijing_interview_after_ba04328e:

    # Gh "Which part of Taiwan are you from?"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:877
translate chinesesim beijing_interview_after_2717873d:

    # fang "Taipei, the capital of the province."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:878
translate chinesesim beijing_interview_after_d3ed068b:

    # Gh "Have you ever been to Kaohshiung?"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:879
translate chinesesim beijing_interview_after_50c98efa:

    # fang "No, I haven't."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:880
translate chinesesim beijing_interview_after_87466338:

    # Gh "I heard it's quite prosperous."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:881
translate chinesesim beijing_interview_after_08327381:

    # fang "Me too."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:882
translate chinesesim beijing_interview_after_d08148d0:

    # fang "What about Uncle Ku?"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:883
translate chinesesim beijing_interview_after_de9e5e90:

    # Gh "You should talk to him."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:884
translate chinesesim beijing_interview_after_8bf178c0:

    # Gh "I'm not sure if he'll go with you."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:885
translate chinesesim beijing_interview_after_b8c752d8:

    # fang "yeah, I guess I will have to talk to him about it."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:886
translate chinesesim beijing_interview_after_d0e729ce:

    # Gh "Want to talk about it later tonight?"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:890
translate chinesesim beijing_interview_after_8e9e39c1:

    # fang "I think it's a good idea."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:893
translate chinesesim beijing_interview_after_2a6a7672:

    # fang "I don't know about this idea."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:895
translate chinesesim beijing_interview_after_d8be2cb4:

    # Gh "Gambling is like water there.{w=1}Isn't it?"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:896
translate chinesesim beijing_interview_after_fbb986b8:

    # "It seems everybody had this stereotype."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:897
translate chinesesim beijing_interview_after_a15cfda5:

    # fang "I never grew up around the casinos."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:898
translate chinesesim beijing_interview_after_175ea7ae:

    # Gh "I guess that's a good thing."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:899
translate chinesesim beijing_interview_after_0723bf35:

    # Gh "What is it like there?"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:900
translate chinesesim beijing_interview_after_2140f739:

    # Gh "I never hear much about it."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:901
translate chinesesim beijing_interview_after_d0aae4e2:

    # fang "It's two Islands with lots of ships."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:902
translate chinesesim beijing_interview_after_61118472:

    # fang "You never see an empty shoreline..{w=1}Like ever."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:903
translate chinesesim beijing_interview_after_368ca954:

    # fang "Even in 1930 when the world was hit by the Great Depression, ships were still fishing."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:904
translate chinesesim beijing_interview_after_a0e35c1a:

    # fang "Fishing is a big thing there."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:905
translate chinesesim beijing_interview_after_fe575714:

    # "Guo Heng listened to what I said with interest."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:906
translate chinesesim beijing_interview_after_f49ad224:

    # fang "The name was a language barrier between the locals and the Portugese arrivers."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:907
translate chinesesim beijing_interview_after_aed81ac6:

    # fang "When asked what this place was called the locals told them {i}Ma-kok{/i}."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:908
translate chinesesim beijing_interview_after_34ece29d:

    # fang "Do you know why?"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:909
translate chinesesim beijing_interview_after_474f4b74:

    # "Guo Heng shook his head."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:910
translate chinesesim beijing_interview_after_35f2b160:

    # fang "A-Ma is the goddess that looked after the harbor and the waters of \"Macau\"."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:911
translate chinesesim beijing_interview_after_d9b4e7a9:

    # fang "They had a well known temple built with her after A-Ma."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:912
translate chinesesim beijing_interview_after_f546b76f:

    # fang "To put it short,{w=1} They thought that the Portugese were asking the name of the Temple which was {i}Ma-Kok{/i} at the time."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:913
translate chinesesim beijing_interview_after_8d0c0b23:

    # "Guo Heng seemed stunned from all of this."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:914
translate chinesesim beijing_interview_after_2234f01e:

    # Gh "That's the same story with Japan."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:915
translate chinesesim beijing_interview_after_6da37066:

    # "We both laughed as he mentioned it."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:916
translate chinesesim beijing_interview_after_93c2f7bf:

    # fang "I also forgot to tell you,"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:917
translate chinesesim beijing_interview_after_8cf29ae0:

    # "Guo Heng settled back down again."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:918
translate chinesesim beijing_interview_after_a2a6bbda:

    # fang "The portugese also imported labour from its colonies in India so Sikh Policeman are common there."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:919
translate chinesesim beijing_interview_after_5cf7c469:

    # fang "THey have a reputation for being bearded and burly."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:920
translate chinesesim beijing_interview_after_34884641:

    # "I began to think what else to tell him."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:921
translate chinesesim beijing_interview_after_81a73b37:

    # fang "Everything else is the same as everywhere else in China."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:922
translate chinesesim beijing_interview_after_693cb46f_1:

    # "Guo Heng nodded."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:923
translate chinesesim beijing_interview_after_dccc845c:

    # Gh "Does Macao have a dialect like Peiping?"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:924
translate chinesesim beijing_interview_after_c81a565a:

    # fang "Yeah, we have Patua."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:925
translate chinesesim beijing_interview_after_b57ceb4e:

    # Gh "Can you speak Patua?"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:926
translate chinesesim beijing_interview_after_15f556b7:

    # fang "A bit."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:927
translate chinesesim beijing_interview_after_607980da:

    # fang "My family can, but I was never taught it. I was only taught Mandarin."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:928
translate chinesesim beijing_interview_after_54830138:

    # fang "If I return, I plan to learn it."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:929
translate chinesesim beijing_interview_after_ccae80fd:

    # Gh "It's nice to keep in touch with your ancestral tongue."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:930
translate chinesesim beijing_interview_after_46d109ba:

    # Gh "My family have lived here for generations."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:931
translate chinesesim beijing_interview_after_0e6b2dc0:

    # Gh "The Peiping Dialect is in my blood."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:932
translate chinesesim beijing_interview_after_28daf52e:

    # Gh "Sorry if you don't understand what we say, force of habit."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:933
translate chinesesim beijing_interview_after_1295c5b3:

    # fang "It's fine. I'm getting used to it."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:934
translate chinesesim beijing_interview_after_8d78a37f:

    # Gh "Is your dialect a mix of Portugese and Cantonese?"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:935
translate chinesesim beijing_interview_after_b9f38572:

    # "I nod to him."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:936
translate chinesesim beijing_interview_after_114cfb81:

    # Gh "Thats good."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:939
translate chinesesim beijing_interview_after_fd6a0ce8:

    # Gh "Kwangchow is an interesting place."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:940
translate chinesesim beijing_interview_after_6f931a71:

    # Gh "By the way, what has the political situation been for Kwangchow like?"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:941
translate chinesesim beijing_interview_after_47be7048:

    # fang "hmmm"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:942
translate chinesesim beijing_interview_after_68b27893:

    # fang "It's gone from Chen Chi-Tang to direct rule under Chiang Chie-Shih."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:943
translate chinesesim beijing_interview_after_be299fdf:

    # fang "So I guess it's united with the central government."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:944
translate chinesesim beijing_interview_after_91c7a324:

    # Gh "That's good to hear."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:945
translate chinesesim beijing_interview_after_01bdf808:

    # Gh "Do you speak the language of Canton?"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:946
translate chinesesim beijing_interview_after_e55fea20:

    # fang "Of course."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:947
translate chinesesim beijing_interview_after_ca3071cc:

    # "Guo Heng nods at me."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:948
translate chinesesim beijing_interview_after_ec191f2d:

    # Gh "I would have learnt it if I had the chance."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:949
translate chinesesim beijing_interview_after_bfd87b32:

    # Gh "How did you learn Mandarin?"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:950
translate chinesesim beijing_interview_after_28291e07:

    # fang "My parents made me learn the national language."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:951
translate chinesesim beijing_interview_after_ec40fc9f:

    # fang "They wanted me to be able to communicate in the national language."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:952
translate chinesesim beijing_interview_after_df9bffdd:

    # "Guo Heng nods to me."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:953
translate chinesesim beijing_interview_after_188b0ad2:

    # fang "Did you know that the founding fathers of the Republic had a debate on the National Language?"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:954
translate chinesesim beijing_interview_after_34fb3b97:

    # "Guo Heng shakes his head."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:955
translate chinesesim beijing_interview_after_961739f8:

    # Gh "No."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:956
translate chinesesim beijing_interview_after_9a26892a:

    # fang "Cantonese lost by one vote.There was an odd number of people in the room. Mandarin became official by one vote."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:957
translate chinesesim beijing_interview_after_d2b6d8dd:

    # "Guo Heng's eyes widened."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:958
translate chinesesim beijing_interview_after_516d278e:

    # Gh "I would speak Cantonese?"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:959
translate chinesesim beijing_interview_after_024b5bea:

    # fang "Eventually, the central government can't enforce the national language yet, not with the threat of war shadowing them."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:960
translate chinesesim beijing_interview_after_ca54bb57:

    # fang "If you recite ancient poetry in Cantonese then it rhymes more than if you would have used Mandarin."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:961
translate chinesesim beijing_interview_after_ffa0f376:

    # fang "People have been researching languages and it seems that Mandarin came around under the Mongol rule which was 700 years ago."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:962
translate chinesesim beijing_interview_after_da6410b7:

    # fang "Cantonese has been estimated to be much older. Like 1900 Years or something."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:963
translate chinesesim beijing_interview_after_00fa6a3c:

    # fang "Don't quote me on it, but what do you think?"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:964
translate chinesesim beijing_interview_after_ccaf98fc:

    # "Guo Heng dabbed his face with the rag."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:965
translate chinesesim beijing_interview_after_558d532f:

    # "He was thinking deeply."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:966
translate chinesesim beijing_interview_after_f029d59c:

    # Gh "It makes sense to me, I think everyone should keep their ancestral tongue with them."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:967
translate chinesesim beijing_interview_after_1dd80252:

    # "Guo Heng flashed a smile."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:968
translate chinesesim beijing_interview_after_4dbb040c:

    # Gh "Teach me some Cantonese some time,{w=1} When I'm off work, preferably."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:972
translate chinesesim beijing_interview_after_43fa0291:

    # fang "I don't mind, it would be nice to teach you."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:973
translate chinesesim beijing_interview_after_e9fb4f84:

    # Gh "Thanks."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:974
translate chinesesim beijing_interview_after_a81d6315:

    # "Guo Heng was strnage that he wanted to learn a dialect he had no practical use for."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:975
translate chinesesim beijing_interview_after_c3b9acae:

    # "but then again he might be like those foreigners who see some gem in the language and learn it to keep a sense of that beauty for themselves."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:978
translate chinesesim beijing_interview_after_ed90ab19:

    # fang "I am not confident when it comes to teaching."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:979
translate chinesesim beijing_interview_after_d58ce64d:

    # "Guo Heng gave a grin and didn't say anything for a while."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:981
translate chinesesim beijing_interview_after_9a4930b5:

    # Gh "That's fine."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:982
translate chinesesim beijing_interview_after_4b2502e3:

    # Gh "Not all knowledge can be taught."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:983
translate chinesesim beijing_interview_after_e1f7b123:

    # "Guo Heng stood up and looked at the wooden table."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:984
translate chinesesim beijing_interview_after_d68734de:

    # Gh "I can't read or write or accomplish anything."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:985
translate chinesesim beijing_interview_after_93e7fb27:

    # Gh "My father sent me to school and I never tried hard."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:986
translate chinesesim beijing_interview_after_7433bc78:

    # Gh "I can barely make out what something says if I'm lucky."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:987
translate chinesesim beijing_interview_after_9ac0cd62:

    # "There was a looming guilty melancholia lodged in his head."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:988
translate chinesesim beijing_interview_after_a639026d:

    # Gh "Can you help me read and write at least?"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:989
translate chinesesim beijing_interview_after_4db82f1c:

    # "I could see his eyes looking teary."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:990
translate chinesesim beijing_interview_after_5560edea:

    # "He was ashamed of himself."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:991
translate chinesesim beijing_interview_after_5eebfc36:

    # Gh "I don't want to fall behind."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:992
translate chinesesim beijing_interview_after_1ac9ae3e:

    # "He whispered it, so quietly it could float and be carried by the wind indefinately."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:993
translate chinesesim beijing_interview_after_5f59ffe4:

    # "At this rate I can't make a choice."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:994
translate chinesesim beijing_interview_after_6d04e264:

    # "I have an oppurtunity."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:997
translate chinesesim beijing_interview_after_9ef040e6:

    # Gh "The capital itself."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:998
translate chinesesim beijing_interview_after_0723bf35_1:

    # Gh "What is it like there?"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:999
translate chinesesim beijing_interview_after_efeb3d78:

    # "Guo Heng applied the rag to his sweaty face again."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1000
translate chinesesim beijing_interview_after_01c6e97e:

    # fang "Nanking in it's name means \"Southern Capital\"."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1001
translate chinesesim beijing_interview_after_9b4607e1:

    # fang "The first time it ruled a united China was in the Ming Dynasty."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1002
translate chinesesim beijing_interview_after_cba0b79d:

    # fang "It's a nice place to live overall."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1003
translate chinesesim beijing_interview_after_1ebff38a:

    # fang "It has a powerful inland port and it's close to Shanghai."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1004
translate chinesesim beijing_interview_after_e3bb646e:

    # fang "If you're rich then you're like a foreigner.{w=1} You dress pretty or look handsome."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1005
translate chinesesim beijing_interview_after_b34779be:

    # fang "There's a lot of people there and you even have western Churches propped up in some places of the city."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1006
translate chinesesim beijing_interview_after_3a8a0d91:

    # fang "Due to these churches, chinese children of Christian believers learn English well and proficiently."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1007
translate chinesesim beijing_interview_after_32d04554:

    # "Guo Heng looked at me bewildered."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1008
translate chinesesim beijing_interview_after_8ddcaae7:

    # Gh "Can you speak English?"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1009
translate chinesesim beijing_interview_after_791942fd:

    # "He looked so excited for some reason."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1010
translate chinesesim beijing_interview_after_839e0226:

    # fang "A bit,{w=1} I can communicate a good amount with some American residents."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1011
translate chinesesim beijing_interview_after_8f663218:

    # Gh "What other kind sof foreigners are there in Nanking?"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1012
translate chinesesim beijing_interview_after_681810b7:

    # "I sat back and thought a little."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1013
translate chinesesim beijing_interview_after_54ff599d:

    # fang "You have German businessmen from the German Reich with their families that settle her to manage German businesses in China."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1014
translate chinesesim beijing_interview_after_ac8ec9c4:

    # fang "Some German military officiers also train Chinese troops, like General Alexander von Falkenhausen."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1015
translate chinesesim beijing_interview_after_8562a356:

    # fang "He is the military advisor to Chiang Chieh-Shih for their joint military reform program."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1016
translate chinesesim beijing_interview_after_a68fd8eb:

    # fang "You have Japanese people too, who initially settled in the Japanese sector of Shanghai and moved to Nanking but you don't see them around too often."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1017
translate chinesesim beijing_interview_after_af87087f:

    # fang "Other europeans are rare to see, apart from some British who do business with Americans."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1018
translate chinesesim beijing_interview_after_f299bf39:

    # Gh "I guess Nanking is becoming a global city."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1019
translate chinesesim beijing_interview_after_c66f9077:

    # fang "I'd say the same thing."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1020
translate chinesesim beijing_interview_after_45c53f1f:

    # Gh "At least foreigners are cooperating rather than invading us.{w=1} Apart from Japan."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1021
translate chinesesim beijing_interview_after_11420a97:

    # "Guo Heng cursed under his breath."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1022
translate chinesesim beijing_interview_after_66611785:

    # "He dabbed the rag once again."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1023
translate chinesesim beijing_interview_after_1ad72abf:

    # Gh "Is there anything special in the capital city of the republic?"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1024
translate chinesesim beijing_interview_after_66037962:

    # fang "There is a statue of Chiang Chieh-Shih in the middle of the city."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1025
translate chinesesim beijing_interview_after_9c5c6a82:

    # Gh "What about Dr Sun?"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1026
translate chinesesim beijing_interview_after_fff38a8e:

    # fang "He doesn't need a statue."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1027
translate chinesesim beijing_interview_after_9e621396:

    # fang "He has a big Mausoleum called \"Sun Yat-sen Mausoleum\" which I think gives his legacy justice."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1028
translate chinesesim beijing_interview_after_550aae89:

    # Gh "What's in the Mausoleum now?"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1029
translate chinesesim beijing_interview_after_24c046c2:

    # fang "A marble sarcophagus."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1030
translate chinesesim beijing_interview_after_9bf3c07b:

    # fang "and a ceiling with the Kuomintang sun stretching above."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1032
translate chinesesim beijing_interview_after_82e4c44b:

    # Gh "In Pu Tong Hua we call it \"Xiang Gang\"."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1033
translate chinesesim beijing_interview_after_04473a26:

    # fang "That's something new I guess."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1034
translate chinesesim beijing_interview_after_126e30df:

    # Gh "What are the British like there?"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1035
translate chinesesim beijing_interview_after_55c6fe68:

    # fang "They segregate themselves from us."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1036
translate chinesesim beijing_interview_after_bba9bc24:

    # fang "The most interaction I see is from horny sailors in the coastal nightclubs and bars."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1037
translate chinesesim beijing_interview_after_46c4e00d:

    # fang "The stumble along the street and puke while they take some girl home for the night."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1038
translate chinesesim beijing_interview_after_7b76a61d:

    # "Guo Heng gave a disapproving nod as heheard this."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1039
translate chinesesim beijing_interview_after_d3832f1a:

    # fang "They do have good schools and insituitions. I learnt English there and read the Holy Bible in Cantonese."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1040
translate chinesesim beijing_interview_after_fe1d70a1:

    # Gh "They translated the bible into Cantonese?"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1041
translate chinesesim beijing_interview_after_24ff21e7:

    # "I give him a light nod."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1042
translate chinesesim beijing_interview_after_8a7e05b5:

    # Gh "That's amazing."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1043
translate chinesesim beijing_interview_after_a3ed6082:

    # fang "I was shocked as well."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1044
translate chinesesim beijing_interview_after_83659870:

    # fang "They made me read the whole thing."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1045
translate chinesesim beijing_interview_after_0a64265a:

    # Gh "Oh I see."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1046
translate chinesesim beijing_interview_after_cb07ca86:

    # Gh "Did you like it?"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1049
translate chinesesim beijing_interview_after_c2be6fcc:

    # fang "Yes I did like it."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1050
translate chinesesim beijing_interview_after_ba26a46b:

    # Gh "How long did it take to finish it？"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1051
translate chinesesim beijing_interview_after_b97836a6:

    # fang "A few years."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1052
translate chinesesim beijing_interview_after_885b92b0:

    # Gh "Was it that long?"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1053
translate chinesesim beijing_interview_after_76dad4dd:

    # fang "I read it many times from front cover to back cover over my schooling years."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1054
translate chinesesim beijing_interview_after_1f87c76b:

    # fang "but I never understood it."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1055
translate chinesesim beijing_interview_after_00292a29:

    # fang "It took me a few years to understand the concept."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1056
translate chinesesim beijing_interview_after_f6d647af:

    # Gh "In God's eyes all humans are born as a fool."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1057
translate chinesesim beijing_interview_after_7ce68b86:

    # "I silently nod to him."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1058
translate chinesesim beijing_interview_after_7f85f433:

    # Gh "I thin there is some higher power."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1059
translate chinesesim beijing_interview_after_fbb9c3da:

    # Gh "I don't think its sentient."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1060
translate chinesesim beijing_interview_after_382acb7c:

    # Gh "The closest thing I consider a God is the good old sun."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1061
translate chinesesim beijing_interview_after_35463e4f:

    # "Guo Heng grimaced as the rays of sunlight painted his face."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1064
translate chinesesim beijing_interview_after_8c5913ac:

    # fang "To be honest...{w=1} I didn't really."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1065
translate chinesesim beijing_interview_after_9a4930b5_1:

    # Gh "That's fine."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1066
translate chinesesim beijing_interview_after_c2591723:

    # Gh "Not everything is for everyone."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1067
translate chinesesim beijing_interview_after_87fe4304:

    # Gh "I normally just keep to myself."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1068
translate chinesesim beijing_interview_after_216f3768:

    # Gh "Sometimes I want to live like Lao Chang."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1069
translate chinesesim beijing_interview_after_96b5f20c:

    # Gh "He loves delivering milk."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1070
translate chinesesim beijing_interview_after_bd99fd06:

    # Gh "but most people like him at one point had to learn to love their situation."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1071
translate chinesesim beijing_interview_after_66568f80:

    # Gh "I will also,{w=1} Some noodle-maker with a small shop, I will learn to love this place more."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1072
translate chinesesim beijing_interview_after_99bf3d75:

    # "There was some strange silence."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1073
translate chinesesim beijing_interview_after_18664c33:

    # "Awkward and ominous."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1074
translate chinesesim beijing_interview_after_e42c9c66:

    # "Guo Heng looks at me with a grin."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1075
translate chinesesim beijing_interview_after_84551679:

    # Gh "Want something to eat later?"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1076
translate chinesesim beijing_interview_after_90773de0:

    # Gh "Its on me."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1079
translate chinesesim beijing_interview_after_b4c0be1e:

    # fang "No no no no!"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1080
translate chinesesim beijing_interview_after_7cdfbf5c:

    # fang "I have enough money to pay for a meal myself."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1081
translate chinesesim beijing_interview_after_5b767510:

    # fang "I think you shouldn't have to treat me for free."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1082
translate chinesesim beijing_interview_after_09f293c4:

    # "We aren't even that close.."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1083
translate chinesesim beijing_interview_after_df77f38d:

    # Gh "you're very adamant , aren't you？"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1084
translate chinesesim beijing_interview_after_5aaaf102:

    # Gh "I'm trying to show my hospitability here!"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1085
translate chinesesim beijing_interview_after_89e2b506:

    # "He gave a warm inviting grin."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1086
translate chinesesim beijing_interview_after_69e26464:

    # Gh "Whenever you feel like it just come up here and it will be on me."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1087
translate chinesesim beijing_interview_after_034e4f86:

    # "I looked up at him."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1088
translate chinesesim beijing_interview_after_c7a47ba0:

    # Gh "Don't worry,{w=1} I make enough to treat people for free."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1091
translate chinesesim beijing_interview_after_bac18827:

    # fang "Sure."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1092
translate chinesesim beijing_interview_after_32da9ece:

    # "Guo Heng smirks at me."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1093
translate chinesesim beijing_interview_after_2b13dc6f:

    # Gh "I hope you aren't fuilt tripping me for free meals."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1094
translate chinesesim beijing_interview_after_40113017:

    # fang "Don't worry Heng, I'm not like that."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1095
translate chinesesim beijing_interview_after_2c2df271:

    # fang "You can trust me after meeting me four times."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1096
translate chinesesim beijing_interview_after_f66d4ab9:

    # Gh "I'm sure you can."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1097
translate chinesesim beijing_interview_after_92ed9675:

    # Gh "Nobody beats me in eatery around here."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1098
translate chinesesim beijing_interview_after_67df61a0:

    # "It seemed Guo Heng took great pride in his line of work."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1100
translate chinesesim beijing_interview_after_b0dd3355:

    # fang "I am from Peiping."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1101
translate chinesesim beijing_interview_after_f47c3b30:

    # Gh "But you seem new to this place."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1102
translate chinesesim beijing_interview_after_84b1ace7:

    # "He was right."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1103
translate chinesesim beijing_interview_after_4f62929f:

    # "Despite the six months I had stayed here with Uncle Ku."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1104
translate chinesesim beijing_interview_after_617db6e8:

    # "I couldn't fit in."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1105
translate chinesesim beijing_interview_after_d980cc7c:

    # "I never knew why..."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1106
translate chinesesim beijing_interview_after_6f962ed1:

    # fang "There's two reasons to explain that."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1107
translate chinesesim beijing_interview_after_2064354a:

    # fang "Firstly my family has a habit of moving across China for different jobs and oppurutunities."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1108
translate chinesesim beijing_interview_after_7114e41a:

    # fang "Secondly all my experience of Peiping was South of it where you could consider it a part of Tientsen.{w=1} So I guess overall I lack much experience if any about living in Peiping."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1109
translate chinesesim beijing_interview_after_b51ad062:

    # Gh "I see then."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1110
translate chinesesim beijing_interview_after_f012d347:

    # Gh "If you need any help anytime..."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1111
translate chinesesim beijing_interview_after_5d7164fb:

    # Gh "I'm here and I'll teach you the ropes of anything."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1112
translate chinesesim beijing_interview_after_2b781c8e:

    # fang "Thanks."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1113
translate chinesesim beijing_interview_after_394ef64f:

    # fang "That means a lot to me here."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1114
translate chinesesim beijing_interview_after_696b9e0b:

    # Gh "I mean you're a guest in our village."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1115
translate chinesesim beijing_interview_after_2cc1817c:

    # Gh "We have to look after you."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1116
translate chinesesim beijing_interview_after_8123f474:

    # fang "Again.{w=1} Thanks."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1117
translate chinesesim beijing_interview_after_4b7fad0c:

    # Gh "On a separate note...{w=0.5} Have you ever drank some {i}Bai-Jiu{/i}?"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1118
translate chinesesim beijing_interview_after_7f7e0077:

    # Gh "Your Great Grandpa's factory sells the good stuff to us, sometimes to some Japanese as well on the other side."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1119
translate chinesesim beijing_interview_after_1f169ede:

    # Gh "You should try it."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1123
translate chinesesim beijing_interview_after_a4b900ad:

    # fang "Surely it can't be that bad."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1124
translate chinesesim beijing_interview_after_51586779:

    # Gh "You got a good spirit in you."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1125
translate chinesesim beijing_interview_after_ca77c936:

    # Gh "Unfortunately my work day isn't over{w=1} and I won't cook while I'm not sober."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1126
translate chinesesim beijing_interview_after_5eed259f:

    # Gh "Meet me up tonight for a drink and we can talk about whatever is on our minds."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1127
translate chinesesim beijing_interview_after_79463e9b:

    # Gh "Especially in regards to the start of a war."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1131
translate chinesesim beijing_interview_after_c82a1013:

    # fang "I don't feel like drinking."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1132
translate chinesesim beijing_interview_after_b961b49a:

    # Gh "Ah well, that's fine."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1133
translate chinesesim beijing_interview_after_8c6384e8:

    # Gh "I respect that."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1134
translate chinesesim beijing_interview_after_690fbde3:

    # Gh "If you need to talk about stuff in your mind just find me after your shift."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1135
translate chinesesim beijing_interview_after_447a50e2:

    # "Guo Heng wiped the rag across his forehead."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1136
translate chinesesim beijing_interview_after_c3f9e448:

    # Gh "You probably need to find a way to get out of here."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1137
translate chinesesim beijing_interview_after_171d4558:

    # Gh "I'm here to help with that."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1138
translate chinesesim beijing_interview_after_cad5f022:

    # fang "I Know..."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1139
translate chinesesim beijing_interview_after_23089930:

    # "Guo Heng really just wanted his brother out of harms way{w=1} which must be the reason why he is trying to support me going away from Peiping."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1144
translate chinesesim Beijing_1_GH_afterward_1e72c33b:

    # "There was nothing to say."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1145
translate chinesesim Beijing_1_GH_afterward_649cd3ba:

    # "It was a bit awkward but it was difficult to articulate conversation."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1146
translate chinesesim Beijing_1_GH_afterward_e2e0d7b8:

    # "I don't have in the slightest an idea of what to do."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1157
translate chinesesim Beijing_1_GH_afterward_856c19c6:

    # fang "I think its time I get moving."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1158
translate chinesesim Beijing_1_GH_afterward_0b5cce5a:

    # fang "I have a pending interview to undergo."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1159
translate chinesesim Beijing_1_GH_afterward_e1c62d0f:

    # fang "He should be coming to conduct it soon."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1160
translate chinesesim Beijing_1_GH_afterward_78beadd1:

    # "Guo Heng rubbed the rag on his face."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1161
translate chinesesim Beijing_1_GH_afterward_fe7c0cb6:

    # Gh "I have to work anyway."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1162
translate chinesesim Beijing_1_GH_afterward_f10776f7:

    # Gh "It was nice to talk with you Fang."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1166
translate chinesesim GH_Beijing_busy_4d31c62a:

    # "Guo Heng put down his sweat rag."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1167
translate chinesesim GH_Beijing_busy_cdd1b889:

    # "The sun was piercing through his thin ragged shirt."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1168
translate chinesesim GH_Beijing_busy_251a43ef:

    # "You could see how the sweat now made the shirt a paint to his canvas of a skin."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1169
translate chinesesim GH_Beijing_busy_87b2224f:

    # Gh "No offence Fang,"
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1170
translate chinesesim GH_Beijing_busy_adb25522:

    # Gh "I have work now."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1171
translate chinesesim GH_Beijing_busy_34eee744:

    # Gh "I can't afford always taking a break."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1172
translate chinesesim GH_Beijing_busy_c742c637:

    # Gh "It takes time to make preparations for a meal."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1174
translate chinesesim GH_Beijing_busy_63876d38:

    # "I grinned at him as well."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1175
translate chinesesim GH_Beijing_busy_1a4ef54e:

    # "It was pleasant."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1176
translate chinesesim GH_Beijing_busy_a951a5d7:

    # "Guo Heng was now slaving away in his kitchen once again."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1177
translate chinesesim GH_Beijing_busy_a4f17af2:

    # "I stood up and walked outside and began to think."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1182
translate chinesesim GH_Beijing_busy_4665c3a6:

    # "I seemed to have a lot of work left."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1183
translate chinesesim GH_Beijing_busy_aef6ec22:

    # "Normally I would be in the urban districts of Peiping where you could see people from all places gather."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1184
translate chinesesim GH_Beijing_busy_46823086:

    # "Even the Japanese across the Marco-Polo Bridge."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1185
translate chinesesim GH_Beijing_busy_472fd45f:

    # "I would attend Peiping university so I could work abroad."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1186
translate chinesesim GH_Beijing_busy_8e5c35cd:

    # "Strangely my dreams seem to force me to run away from my current situation."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1187
translate chinesesim GH_Beijing_busy_0b7f1503:

    # "I begin to walk back home as my books are there."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1188
translate chinesesim GH_Beijing_busy_308e9a83:

    # "How would I confront Uncle Ku?"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1189
translate chinesesim GH_Beijing_busy_e570126b:

    # "I felt like I has strained things again."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1190
translate chinesesim GH_Beijing_busy_8151f349:

    # "Part of me always does this."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1191
translate chinesesim GH_Beijing_busy_0987d880:

    # "Part of me always blames me for blaming myself for things outside of my control."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1192
translate chinesesim GH_Beijing_busy_571125c7:

    # "I realize I always dwell on such trivial matters."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1193
translate chinesesim GH_Beijing_busy_90d217a6:

    # "It's strange really."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1194
translate chinesesim GH_Beijing_busy_0c49fe16:

    # "I guess this walk didn't get me anything."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1195
translate chinesesim GH_Beijing_busy_2a97822d:

    # "I felt my leather shoes grind the dust as I ambled."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1196
translate chinesesim GH_Beijing_busy_1f3f4d17:

    # "Is my education even a priority?"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1197
translate chinesesim GH_Beijing_busy_33b15cff:

    # "{i}I should be evacuating... right?{/i}"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1198
translate chinesesim GH_Beijing_busy_2c663bf0:

    # "The Chang San Brothel was now in view again."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1199
translate chinesesim GH_Beijing_busy_9c36687f:

    # "Chang San was not even its name, it was just a type of brothel."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1200
translate chinesesim GH_Beijing_busy_2c956038:

    # "I never bothered asking the name anyway."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1201
translate chinesesim GH_Beijing_busy_b96a3252:

    # "Was it important to know its name?"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1202
translate chinesesim GH_Beijing_busy_3a3fafb6:

    # "I began to speed-walk at this rate."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1203
translate chinesesim GH_Beijing_busy_7671fde0:

    # "The wine factory that was supposed to be starting up soon will now be left decrepit."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1204
translate chinesesim GH_Beijing_busy_4b284c7e:

    # "I wonder what the soldiers would do to it when they occupy it."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1205
translate chinesesim GH_Beijing_busy_325ce55b:

    # "Yue-Lan has already expressed what the soldiers would do with the brothel under occupation."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1206
translate chinesesim GH_Beijing_busy_37fe236f:

    # "It didn't strike me as surprising in any sense."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1207
translate chinesesim GH_Beijing_busy_e6ef6aff:

    # "They would probably start up the factory with some volunteers and drink till they drop."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1208
translate chinesesim GH_Beijing_busy_d16faf6a:

    # "I kept on walking for what felt like 10 minutes until the pathetic pond came back into view."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1209
translate chinesesim GH_Beijing_busy_a1784e35:

    # "The pond in my eyes resembled an animal that was wounded and in pain."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1210
translate chinesesim GH_Beijing_busy_0ae80886:

    # "Like it needed to be put out of its misery."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1211
translate chinesesim GH_Beijing_busy_22d4ad36:

    # "It would also be a breeding ground for ill stuff."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1212
translate chinesesim GH_Beijing_busy_4bd6f52a:

    # "The water is brown and god knows that is swimming in there."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1213
translate chinesesim GH_Beijing_busy_c0959c7c:

    # "I gaze away and walk back in through the Da-men."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1214
translate chinesesim GH_Beijing_busy_9c2be52e:

    # "I couldn't hear Uncle Ku doing anything at the alter."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1215
translate chinesesim GH_Beijing_busy_1c1d9810:

    # "I wonder if he is home."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1216
translate chinesesim GH_Beijing_busy_aa442b50:

    # "I have to find my books and start reading."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1217
translate chinesesim GH_Beijing_busy_87021347:

    # "I chose courses in law at Peking university."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1218
translate chinesesim GH_Beijing_busy_dd53b319:

    # "I seemed to have a strong sense of civil justice since I was young."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1219
translate chinesesim GH_Beijing_busy_c426fce9:

    # "When you're a kid and you feel like the leadership has failed you then you grow up wanting that to change."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1220
translate chinesesim GH_Beijing_busy_af38275d:

    # "Perhaps my determination to change a faulty system came from a hatred of its failures."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1221
translate chinesesim GH_Beijing_busy_9266752a:

    # "I can't even understand my own intentions for such ambitions but I persist."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1222
translate chinesesim GH_Beijing_busy_fe6b199f:

    # "I reject the alternate options."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1223
translate chinesesim GH_Beijing_busy_58afbd8f:

    # "I dragged a book off of the shelf and put it on my table."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1224
translate chinesesim GH_Beijing_busy_a90da91c:

    # "One day I wish I could become part of this system and hopefully steer it towards a path for the better."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1225
translate chinesesim GH_Beijing_busy_e3596e3a:

    # "Time to study."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1228
translate chinesesim GH_Beijing_busy_fc8a7617:

    # "I recited the final quotes and notes to cement them in my head."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1229
translate chinesesim GH_Beijing_busy_5c6506ac:

    # "Studying like this has helped me become more calm."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1230
translate chinesesim GH_Beijing_busy_f09bc991:

    # "It's all I have these days."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1231
translate chinesesim GH_Beijing_busy_0ac34fe7:

    # "Working towards more work."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1232
translate chinesesim GH_Beijing_busy_ef7ed6db:

    # "{i}I'm such a workaholic{/i}"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1233
translate chinesesim GH_Beijing_busy_3cf8f5fa:

    # "Uncle Ku still hasn't come back even despite the fact I had studied to just above an hour."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1234
translate chinesesim GH_Beijing_busy_ded365d6:

    # "I wonder if he was called to a meeting."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1235
translate chinesesim GH_Beijing_busy_42407757:

    # "Surely I would meet them on the way to Guo's shop."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1236
translate chinesesim GH_Beijing_busy_d3d30bc7:

    # "Perhaps he was talking with some workers on building air raid shelters."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1237
translate chinesesim GH_Beijing_busy_a3705f63:

    # "Uncle Ku is well connected with people in high places, perhaps an officer informed him to undertake responsibility to dig air raid shelters."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1238
translate chinesesim GH_Beijing_busy_0e8a720c:

    # "After all they might start bombing any day soon when they're mobilized."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1239
translate chinesesim GH_Beijing_busy_dce7b468:

    # "The thought of war..."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1240
translate chinesesim GH_Beijing_busy_1b06a381:

    # "It still haunts me."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1241
translate chinesesim GH_Beijing_busy_36ed8b4d:

    # "Will people be able to keep their sanity after witnessing near death, bloodshed and regular bombing?"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1242
translate chinesesim GH_Beijing_busy_186e9373:

    # "In all honesty I have doubts on my capacity to handle the terrors of war."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1243
translate chinesesim GH_Beijing_busy_49fc179a:

    # "I'm not some mythological hero who can fight till their last breath."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1244
translate chinesesim GH_Beijing_busy_50197ca6:

    # "I just didn't want to be involved in this."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1245
translate chinesesim GH_Beijing_busy_83c33747:

    # "{i}Is that why I want to run away?{/i}"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1246
translate chinesesim GH_Beijing_busy_30cb46e9:

    # "{i}If Uncle Ku and Professor Po realized this then this support for escaping further south makes sense.{/i}"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1247
translate chinesesim GH_Beijing_busy_face8a8a:

    # "Beijing may be the resident of my ancestors but I have to face reality."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1248
translate chinesesim GH_Beijing_busy_704e33dd:

    # Gu "Fang?"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1249
translate chinesesim GH_Beijing_busy_0d844738:

    # "I sat up not expecting him to enter the house so silently."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1251
translate chinesesim GH_Beijing_busy_f7ba3e8a:

    # Gu "Ever since this whole conflict broken out I have noticed you are much more...{i}stressed{/i}"
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1252
translate chinesesim GH_Beijing_busy_31bfbf27:

    # Gu "I know it was my fault for getting the professor drunk and I'm sorry for that."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1253
translate chinesesim GH_Beijing_busy_30108bf4:

    # Gu "I honestly intend to help you."
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1254
translate chinesesim GH_Beijing_busy_bc17bae4:

    # Gu ""
    Gu ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1260
translate chinesesim GH_Beijing_busy_5b9e1bab:

    # "Uncle Cheung's silk house was an extraordinary beauty."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1261
translate chinesesim GH_Beijing_busy_d8511364:

    # "The type poets die away trying to express on paper."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1262
translate chinesesim GH_Beijing_busy_74bc8408:

    # "In the house full of buckling wooden levers and rods were colorful rags hanging down like the dress of an angel."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1263
translate chinesesim GH_Beijing_busy_df00867f:

    # "A lake of dye would sit in the middle where one would use it to well...{nw}"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1264
translate chinesesim GH_Beijing_busy_4c91e0c4:

    # "dye clothes."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1265
translate chinesesim GH_Beijing_busy_032ffcb1:

    # "Uncle Cheung's silk house wasn't too far from Guo Heng's shop."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1266
translate chinesesim GH_Beijing_busy_ebc61ff4:

    # "It was on the way back home but diverging from the brothel."
    ""

translate chinesesim strings:

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:29
    old "Yes I am Ready."
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:29
    old "Please Give me a minute to think."
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:62
    old "Peiping"
    new "北京"

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:62
    old "Nanking"
    new "南京"

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:62
    old "Kwangchow"
    new "广州"

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:62
    old "Hong-Kong"
    new "香港"

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:62
    old "Macau"
    new "澳门"

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:62
    old "Taiwan"
    new "台湾"

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:161
    old "Hong Kong"
    new "香港"

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:221
    old "Attending school as a student."
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:221
    old "Working as a scholarly apprentice under my uncle"
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:221
    old "Working here to send money back home."
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:221
    old "Taking a break after military conscription training"
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:325
    old "Taoism and Confucianism"
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:325
    old "Buddhism"
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:325
    old "Christianity"
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:398
    old "Yes"
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:398
    old "No"
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:443
    old "no"
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:599
    old "Chiang Chie Shih"
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:599
    old "Wang Ching Wei"
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:599
    old "Li Tsung-Jen"
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:599
    old "Mao Tse-Tung"
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:599
    old "I don't know"
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:887
    old "I might consider it."
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:887
    old "I don't think so."
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:969
    old "Sure"
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:969
    old "Sorry, I can't"
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1120
    old "Sure, I'll try"
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1120
    old "No thanks, I'm fine"
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1149
    old "Keep conversing with Guo Heng."
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1149
    old "Take leave"
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1178
    old "Catch up on work for school."
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1178
    old "Go to Cheung's silk-house to work"
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1178
    old "Visit Shuang-lin temple"
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_1937_sino_japanese_war_bookmark.rpy:1178
    old "Go home"
    new ""

