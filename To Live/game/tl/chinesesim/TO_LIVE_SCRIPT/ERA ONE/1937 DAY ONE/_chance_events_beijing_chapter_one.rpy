﻿# TODO: Translation updated at 2020-06-02 14:36

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:9
translate chinesesim GH_timed_oppurtunity_2ef8d23b:

    # fang "I believe in you."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:10
translate chinesesim GH_timed_oppurtunity_36f98100:

    # fang "I can teach you everything you need to know."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:11
translate chinesesim GH_timed_oppurtunity_65516521:

    # "Guo Heng's face lit up."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:12
translate chinesesim GH_timed_oppurtunity_acb1c90d:

    # "You could see, a faint shimmer of hope in his eyes."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:13
translate chinesesim GH_timed_oppurtunity_b51a388e:

    # "He clasped his hands and bowed."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:14
translate chinesesim GH_timed_oppurtunity_87829eae:

    # Gh "Thank you so much."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:15
translate chinesesim GH_timed_oppurtunity_8acc00b6:

    # "He kept whispering the words to me."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:16
translate chinesesim GH_timed_oppurtunity_4a35e334:

    # "I couldn't comprehend this situation."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:17
translate chinesesim GH_timed_oppurtunity_2ad2e01b:

    # "What was upsetting him."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:18
translate chinesesim GH_timed_oppurtunity_13a5bf76:

    # "What could language offer him."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:19
translate chinesesim GH_timed_oppurtunity_d4fc2b34:

    # "Why am I involved?"
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:20
translate chinesesim GH_timed_oppurtunity_96357bb8:

    # "Questions swirled in my head as Guo Heng sat back at the table."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:21
translate chinesesim GH_timed_oppurtunity_616bffdd:

    # "He wiped his teary eyes with his dirt painted sleeves."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:22
translate chinesesim GH_timed_oppurtunity_c1581e46:

    # Gh "Fang this means so much to me."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:23
translate chinesesim GH_timed_oppurtunity_06f7c1e2:

    # Gh "Sorry for getting so worked up."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:24
translate chinesesim GH_timed_oppurtunity_e262d01f:

    # Gh "Stuff like this happens you know."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:28
translate chinesesim GH_timed_oppurtunity_d90b5742:

    # "I couldn't directly refuse him."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:29
translate chinesesim GH_timed_oppurtunity_9b1df341:

    # "I could however console him."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:30
translate chinesesim GH_timed_oppurtunity_e6beab33:

    # "I had a feeling he had a sense of self hate for failing his father academically."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:31
translate chinesesim GH_timed_oppurtunity_08fe3a18:

    # "His father probably worked hard to educate him and now he can't move on from regrets."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:32
translate chinesesim GH_timed_oppurtunity_f78a8803:

    # "Guo Heng could only look at me."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:33
translate chinesesim GH_timed_oppurtunity_4020311d:

    # fang "Don't overthink."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:34
translate chinesesim GH_timed_oppurtunity_c3264831:

    # fang "Have a seat."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:35
translate chinesesim GH_timed_oppurtunity_a49481ab:

    # "I felt like I was talking to some distraught older brother."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:36
translate chinesesim GH_timed_oppurtunity_c3264831_1:

    # fang "Have a seat."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:37
translate chinesesim GH_timed_oppurtunity_19011b01:

    # "I urged him once again."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:38
translate chinesesim GH_timed_oppurtunity_96c6a0e7:

    # "Guo Heng glanced at the wooden bench and gradually leveled down to sit on it."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:39
translate chinesesim GH_timed_oppurtunity_e09f3546:

    # fang "I don't have confidence to teach."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:40
translate chinesesim GH_timed_oppurtunity_e90c7905:

    # fang "I can't be anyone's bastion of hope."
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:41
translate chinesesim GH_timed_oppurtunity_7dcc1c32:

    # "Guo Heng listened patiently."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:42
translate chinesesim GH_timed_oppurtunity_f44c8ba6:

    # "I had to make him understand my view."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:43
translate chinesesim GH_timed_oppurtunity_fa58f198:

    # Gh "You're very wise Fang."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:44
translate chinesesim GH_timed_oppurtunity_e50ffafc:

    # "Guo Heng seemed more calm and collected now."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:45
translate chinesesim GH_timed_oppurtunity_b1237c71:

    # "He sat as if he didn't go through what he did now."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:46
translate chinesesim GH_timed_oppurtunity_727ef670:

    # "He championed his hiding abilities."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:51
translate chinesesim GH_timed_oppurtunity_slow_c158564b:

    # "Guo Heng was still teary."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:52
translate chinesesim GH_timed_oppurtunity_slow_21bee341:

    # "My body was frozen."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:53
translate chinesesim GH_timed_oppurtunity_slow_7878a902:

    # "I didn't know how to approach this situation."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:54
translate chinesesim GH_timed_oppurtunity_slow_be524839:

    # "I have never seen anyone here to emotional."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:55
translate chinesesim GH_timed_oppurtunity_slow_1d5eed10:

    # "Perhaps I had been{w=1} selfish."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:56
translate chinesesim GH_timed_oppurtunity_slow_49d012b8:

    # "Guo Heng wiped his teary wet eyes into his dirt painted ragged sleeves."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:57
translate chinesesim GH_timed_oppurtunity_slow_5d39e3b6:

    # "He clenched his fists and unclenched as he sat back down at the table."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:58
translate chinesesim GH_timed_oppurtunity_slow_43346672:

    # Gh "Forget it."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:59
translate chinesesim GH_timed_oppurtunity_slow_8dfb9045:

    # "I could tell his strength was a facade."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:60
translate chinesesim GH_timed_oppurtunity_slow_30df2c2d:

    # "Something was breaking him into pieces inside."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:61
translate chinesesim GH_timed_oppurtunity_slow_2002f69e:

    # "I'm just too selfish to notice."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:62
translate chinesesim GH_timed_oppurtunity_slow_a41c2111:

    # fang "I'm sorry Guo-{nw}"
    fang ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:63
translate chinesesim GH_timed_oppurtunity_slow_9a2d6f21:

    # Gh "Leave it."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:64
translate chinesesim GH_timed_oppurtunity_slow_d7c8681d:

    # Gh "Don't talk about it."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:65
translate chinesesim GH_timed_oppurtunity_slow_a5211cfa:

    # "Guo Heng seemed to want to forget about it."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:66
translate chinesesim GH_timed_oppurtunity_slow_3dfbabe9:

    # "I wasn't sure why he had so much self hate for not studying well."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:67
translate chinesesim GH_timed_oppurtunity_slow_5ac1cded:

    # "Perhaps he felt he failed his father."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:68
translate chinesesim GH_timed_oppurtunity_slow_9138246c:

    # "I wasn't much different as well."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:69
translate chinesesim GH_timed_oppurtunity_slow_f7b3cbd2:

    # "He was quite and more collected."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:70
translate chinesesim GH_timed_oppurtunity_slow_36f44332:

    # "I could tell a difference in the atmosphere."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:78
translate chinesesim GH_first_goodbye_d35044a1:

    # "Guo Heng stood up."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:80
translate chinesesim GH_first_goodbye_100ec951:

    # Gh "I hope you come for that meal I promised."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:81
translate chinesesim GH_first_goodbye_2e992cf2:

    # "He have a cheerful grin to me."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:83
translate chinesesim GH_first_goodbye_4dc92c54:

    # Gh "I look forward to learning new things."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:84
translate chinesesim GH_first_goodbye_153c44e5:

    # "He have a cheerful grin."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:86
translate chinesesim GH_first_goodbye_1a3b353a:

    # Gh "I look forward to helping you out if you come to talk tonight."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:87
translate chinesesim GH_first_goodbye_5f1d9b89:

    # "He gave a cheerful grin."
    ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:89
translate chinesesim GH_first_goodbye_ec7bb20c:

    # Gh "Join me for some beer."
    Gh ""

# game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:90
translate chinesesim GH_first_goodbye_b788eb24:

    # "He pointed to a distilled alcoholic bottle sitting inside his work kitchen."
    ""

translate chinesesim strings:

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:6
    old "Don't worry I can."
    new ""

    # game/TO_LIVE_SCRIPT/ERA ONE/1937 DAY ONE/_chance_events_beijing_chapter_one.rpy:6
    old "Don't sweat it"
    new ""

