##     ##    ###    ########  ##      ##  #######  ########  ##    ## 
###   ###   ## ##   ##     ## ##  ##  ## ##     ## ##     ## ##   ##  
#### ####  ##   ##  ##     ## ##  ##  ## ##     ## ##     ## ##  ##   
## ### ## ##     ## ########  ##  ##  ## ##     ## ########  #####    
##     ## ######### ##        ##  ##  ## ##     ## ##   ##   ##  ##   
##     ## ##     ## ##        ##  ##  ## ##     ## ##    ##  ##   ##  
##     ## ##     ## ##         ###  ###   #######  ##     ## ##    ## 


    # TODO: Translation updated at 2020-06-05 12:37

translate chinesesim strings:

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:158
    old "Republic of China"
    new "中国民国"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:159
    old "Chinese Soviet Republic"
    new "中华苏维埃共和国"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:161
    old "Empire of Japan"
    new "日本帝国"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:162
    old "Empire of Manchukuo"
    new "大满洲帝国"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:163
    old "Reorganized National Government of the Republic of China"
    new "中华民国国民政府"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:165
    old "Shaanxi Clique"
    new "晋系"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:166
    old "Hsi Pei San Ma"
    new "西北三马"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:167
    old "KwangHsi Clique"
    new "新桂系"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:168
    old "Yunnan Clique"
    new "滇系"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:169
    old "Shandong Clique"
    new "鲁系"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:170
    old "Hunan Clique"
    new "湘系"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:171
    old "Szechewan Clique"
    new "川军"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:172
    old "Sikang Clique"
    new "西康省"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:173
    old "Sinkiang Clique"
    new "新疆军阀"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:174
    old "Kingdom of Tibet"
    new "西藏"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:175
    old "East Hopeh autonomous\nanti-communism council"
    new "冀东\n防共自治政府"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:176
    old "Mongol United\nAutonomous Government"
    new "蒙疆聯合自治政府"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:178
    old "United Kingdom"
    new "英国"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:179
    old "Portugese Republic"
    new "葡萄牙共和国"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:180
    old "French Republic"
    new "法兰西共和国"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:181
    old "Union of\nSoviet Socialist Republics"
    new "苏维埃\n社会主义共和国联盟"

# TODO: Translation updated at 2020-06-05 13:19

translate chinesesim strings:

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:69
    old "Kwang-Chow Wan"
    new "广州湾"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:70
    old "Shanghai"
    new "上海"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:72
    old "Yulin"
    new "玉林"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:73
    old "Foochow"
    new "福州"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:74
    old "Ning-Po"
    new "宁波"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:75
    old "Nan-Kang"
    new "南康"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:76
    old "An-King"
    new "安庆"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:77
    old "Tientsin"
    new "天津"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:78
    old "Tsingtao"
    new "青岛"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:79
    old "Wei-Hai-Wei"
    new "威海"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:80
    old "Da-ren"
    new "大连"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:82
    old "Soochow"
    new "苏州"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:83
    old "Tai Ping"
    new "太平"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:84
    old "Chang Chow"
    new "漳州"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:85
    old "Sian"
    new "西安"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:86
    old "Kaohshiung"
    new "高雄"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:87
    old "Taipei"
    new "台北"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:88
    old "Mukden"
    new "沈阳"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:89
    old "Yantai"
    new "烟台"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:90
    old "Weifang"
    new "潍坊"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:91
    old "Lini"
    new "临沂"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:92
    old "Tsining"
    new "济宁"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:93
    old "Paoting"
    new "保定"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:94
    old "Yancheng"
    new "盐城"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:95
    old "Shihkiachwuang"
    new "暑假装"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:96
    old "Lianyun"
    new "联运"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:97
    old "Taichow"
    new "台州"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:98
    old "Wenchow"
    new "温州"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:99
    old "Tungkuan"
    new "东莞"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:100
    old "Yuengkong"
    new "阳江"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:101
    old "Swabue"
    new "汕尾"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:102
    old "Kian"
    new "吉安"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:103
    old "Kingwha"
    new "金华"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:104
    old "Shangkiu"
    new "商丘"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:105
    old "Nanyang"
    new "南阳"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:106
    old "Taiyuan"
    new "太原"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:107
    old "Linfen"
    new "临汾"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:108
    old "Tatung"
    new "大同"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:109
    old "Siangyang"
    new "襄阳"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:110
    old "Siaokan"
    new "孝感"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:111
    old "Yicheng"
    new "宜城"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:112
    old "Hengyang"
    new "衡阳"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:113
    old "Hwaihwa"
    new "怀化"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:114
    old "Hochi"
    new "河池"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:115
    old "kweikang"
    new "贵港"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:116
    old "Pakhoi"
    new "北海"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:117
    old "Tsunyi"
    new "遵义"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:118
    old "Luchow"
    new "泸州"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:119
    old "Bachung"
    new "巴中"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:120
    old "Fusze"
    new "延安"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:121
    old "Ordos"
    new "鄂尔多斯"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:122
    old "Bayan Nur"
    new "巴彦淖尔"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:123
    old "Yuchwan"
    new "银川"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:124
    old "Axla"
    new "阿拉善盟"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:125
    old "Tenshui"
    new "天水"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:126
    old "Lanchow"
    new "兰州"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:127
    old "Wuwei"
    new "武威"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:128
    old "Kiuquan"
    new "酒泉"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:129
    old "Deqen"
    new "德钦"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:130
    old "Puerh"
    new "普洱"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:131
    old "Chinchew"
    new "泉州"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:132
    old "Sanming"
    new "三明"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:133
    old "Chinwangtao"
    new "秦皇岛"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:134
    old "Hulutao"
    new "葫芦岛"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:135
    old "Yingkow"
    new "营口"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:136
    old "Chaoyang"
    new "朝阳"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:137
    old "Chifeng"
    new "赤峰"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:138
    old "Hsinking"
    new "长春"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:139
    old "Tungliao"
    new "通辽"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:140
    old "Hulun Buir"
    new "呼伦贝尔"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:141
    old "Manchowli"
    new "满洲里"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:142
    old "Tsitsihar"
    new "齐齐哈尔"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:143
    old "Kiamusze"
    new "佳木斯"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:144
    old "Mutankiang"
    new "牡丹江"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:145
    old "Kashgar"
    new "喀什"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:146
    old "Karamay"
    new "克拉玛依"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:147
    old "Altay"
    new "阿勒泰地"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:148
    old "Urumqi"
    new "乌鲁木齐"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:167
    old "Generalissimo President of the Republic"
    new "大元帅共和国总统"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:167
    old "Conservative Junta"
    new "保守军政府"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:167
    old "Central Government of Kuomintang"
    new "中央国民党政府"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:167
    old "Kuomintang"
    new "国名党"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:167
    old "Second United Front"
    new "国共关系"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:168
    old "Revolutionary Marxist Leader"
    new "革命马克思主义者"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:168
    old "Marxist Guerillas"
    new "马克思主义游击队"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:168
    old "International Marxism"
    new "国际马克思主义"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:168
    old "Chung-kuo Kung-ch'an-tang"
    new "中国共产党"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:170
    old "Emperor Hirohito"
    new "昭和天皇"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:170
    old "Emperor of Japan"
    new "大日本帝国的天帝"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:170
    old "Fascist Monarchy"
    new "法西斯君主制"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:170
    old "Co-Prosperity Sphere"
    new "共同繁荣领域"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:170
    old "Tohokai"
    new "東方会"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:170
    old "Co-Properity Sphere"
    new "大东亚共荣圈"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:171
    old "Aisin Gioro Pu-Yi"
    new "爱新觉罗 溥仪"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:171
    old "Collaborationist Emperor of Manchukuo"
    new "满洲国合作党领袖"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:171
    old "Fascist Puppet Monarchy"
    new "法西斯伪君主制"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:171
    old "Manchōwkuó Hsiéhehuì"
    new "满洲国协和会"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:172
    old "Wang Ching-Wei"
    new "汪精卫"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:172
    old "Japanese Collaborationist Dictator"
    new "日本协作主义者独裁者"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:172
    old "Fascist Puppet State"
    new "法西斯伪政府"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:172
    old "Left Kuomintang"
    new "左国民党"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:174
    old "Yan HsiShan"
    new "阎锡山"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:174
    old "Warlord"
    new "军阀"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:175
    old "Ma Bu Fang"
    new "马步芳"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:176
    old "Li Tsun-Jen"
    new "李宗仁"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:177
    old "Lung Yun"
    new "龙云"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:178
    old "Han Fuju"
    new "韩复榘"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:179
    old "Chang Chih Chung"
    new "张治中"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:180
    old "Liu Hsiang"
    new "刘湘"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:181
    old "Liu Wen Hui"
    new "刘文辉"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:182
    old "ShengShih Ts'ai"
    new "盛世才"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:182
    old "Internationale & Central Government of Kuomintang"
    new "国际马克思主义 和 国民党中央政府"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:183
    old "Jamphel Yeshe Gyaltsen"
    new "詹普尔·耶什·贾尔森"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:183
    old "Fifth Reting Rinpoche"
    new "第五仁波切"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:183
    old "Monastic Monarchy"
    new "修道院君主制"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:183
    old "Non-Aligned"
    new "不结盟"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:183
    old "Dalai Lama"
    new "达赖喇嘛"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:184
    old "Yin-Ju-Keng"
    new "殷汝耕"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:184
    old "Buffer State"
    new "缓冲国家"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:184
    old "East Hopei Autonomous Government"
    new "冀东\n防共自治政府"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:185
    old "Prince Demchugdongrub"
    new "德穆楚克栋鲁普"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:185
    old "Mongol Military Government"
    new "蒙疆聯合自治政府"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:187
    old "Neville Chamberlain"
    new "内维尔·张伯伦"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:187
    old "Prime Minister of United Kingdom"
    new "英国首相"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:187
    old "Imperial Federation"
    new "殖民帝国"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:187
    old "League of Nations"
    new "国际联盟"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:187
    old "Conservative Party"
    new "保守党"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:187
    old "Anglo-French Alliance"
    new "国际联盟"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:188
    old "Antonio de Oliveira Salazar"
    new "安東尼奧·德·奧利維拉·薩拉查"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:188
    old "Prime Minister of Portugal"
    new "葡萄牙总理"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:188
    old "Authoritarian Oligarchic Republic"
    new "威权专制共和国"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:188
    old "Estada Novo"
    new "新國家 (葡萄牙)"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:188
    old "Uniao Nacional"
    new "国民联盟"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:188
    old "No Faction"
    new "没有联盟"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:189
    old "Camille Chautemps"
    new "卡米耶·肖当"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:189
    old "Prime Minister of France"
    new "法国总理"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:189
    old "Democratic Republic"
    new "民主共和国"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:189
    old "Parti radical"
    new "激进党"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:190
    old "Joseph Stalin"
    new "约瑟夫·斯大林"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:190
    old "General Secretary of the Communist Party of the Soviet Union"
    new "苏联共产党总书记"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:190
    old "Marxist Dictatorship"
    new "马克思主义专政"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:190
    old "Kommunisticheskaya partiya\nSovetskogo Soyuza"
    new "苏联共产党"

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:190
    old "Comintern"
    new "共产国际"

# TODO: Translation updated at 2020-06-05 23:36

translate chinesesim strings:

    # game/TO_LIVE_DEFINES/TL_defines_mapping.rpy:182
    old "Soviet Junta"
    new "苏联军政府"

