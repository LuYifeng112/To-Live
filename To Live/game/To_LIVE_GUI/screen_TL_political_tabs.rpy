default nation = None

screen political_menu():
    tag menu
    default quote = renpy.random.choice(TL_quotes)

    key "K_SPACE" action ShowMenu(main_menu)
    key "K_TAB" action ShowMenu(main_menu)
    key "K_ESCAPE" action Return()
    key "o" action Return()
    key "n" action ShowMenu('guo_map')
    key "1" action ShowMenu('guo_map')
    key "g" action ShowMenu('glossary')
    key "p" action ShowMenu('poems')
    key "l" action ShowMenu('historical_event_log')
    key "2" action ShowMenu('glossary')
    key "3" action ShowMenu('poems')
    key "4" action ShowMenu('historical_event_log')

    add "#141414"

    text "[objective]" xalign 0.05 yalign 0.05 size 20 font "fonts/eng_moria/MoriaCitadel.ttf" at slant
    text "[subtext]" xalign 0.07 yalign 0.15 size 12 font "fonts/eng_moria/MoriaCitadel.ttf" at slant_st
    text "[quote]" xpos 45 yalign 1.1 size 15 font "fonts/eng_moria/MoriaCitadel.ttf" color "#000000" at slant_q
    text __("Pause") xalign 0.4 size 55 font "fonts/eng_moria/MoriaCitadel.ttf" at slant_title

    textbutton __("National Map") xalign 0.9 yalign 0.45 action ShowMenu('guo_map') at am_map
    textbutton __("Historical Event Log") xalign 0.91 yalign 0.55 action ShowMenu('historical_event_log') at am_hist
    textbutton __("Save Game") xalign 0.3 yalign 0.35 action ShowMenu('save') at ambient_left
    textbutton __("Load Game") xalign 0.3 yalign 0.45 action ShowMenu('load') at ambient_left
    textbutton __("Dialogue History") xalign 0.3 yalign 0.55 action ShowMenu('history') at ambient_left
    textbutton __("Main Menu") xalign 0.3 yalign 0.25 action ShowMenu('main_menu') at ambient_left
    textbutton __("Character profiles") xalign 0.92 yalign 0.65 at am_ch
    textbutton __("Resistance Journal") xalign 0.93 yalign 0.75 at am_res

screen fang_character():
    tag menu
    key "K_SPACE" action ShowMenu(main_menu)
    key "K_TAB" action ShowMenu(main_menu)
    key "K_ESCAPE" action Return()
    key "n" action ShowMenu('guo_map')
    key "1" action ShowMenu('guo_map')
    key "g" action ShowMenu('glossary')
    key "p" action ShowMenu('poems')
    key "l" action ShowMenu('historical_event_log')
    key "2" action ShowMenu('glossary')
    key "3" action ShowMenu('poems')
    key "4" action ShowMenu('historical_event_log')

screen guo(): 
    tag menu
    default vbox_button_is_hovered = False
    key "K_SPACE" action ShowMenu(main_menu)
    key "K_TAB" action ShowMenu(main_menu)
    key "K_ESCAPE" action Return()
    key "n" action ShowMenu('guo_map')
    key "1" action ShowMenu('guo_map')
    key "g" action ShowMenu('glossary')
    key "p" action ShowMenu('poems')
    key "l" action ShowMenu('historical_event_log')
    key "2" action ShowMenu('glossary')
    key "3" action ShowMenu('poems')
    key "4" action ShowMenu('historical_event_log')
    imagebutton:
        idle "gui/button_return_icon.png"
        hover "gui/button_return_icon_hovered.png"
        xalign 0.9 yalign 0.05
        action Return()
    mousearea: #This is to make sure that the list doesn't block info or look ugly unless we want to select a new country
        area (0, 0, 500, 1080)
        hovered SetScreenVariable("vbox_button_is_hovered", True)
        unhovered SetScreenVariable("vbox_button_is_hovered", False)        
    window:
        style "gm_root"
        add "#141414"
    viewport:
        xysize (config.screen_width, config.screen_height)
        child_size (2000, 1100)
        draggable True
        edgescroll (200, 200)
        showif vbox_button_is_hovered:
            vbox spacing 15:
                at fader
                for n in TL_GUO:
                    if n.IsActive:
                        textbutton n.name:
                            action SetVariable("nation", n)                    
    text [nation.name] xalign 0.5 ypos 10 size 40 font "fonts/eng_moria/MoriaCitadel.ttf" at slant_guo_name:
        if _preferences.language == "chinesesim":
            font "fonts/chi_cities/MaShanZheng-Regular.ttf"
            size 60
        elif _preferences.language == "chinese":
            font "fonts/chi_genkai/Genkaimincho.ttf"
            size 60
    text [nation.leader] xalign 0.25 yalign 0.2 size 25 font "fonts/eng_moria/MoriaCitadel.ttf":
        if _preferences.language == "chinesesim":
            font "fonts/chi_cities/MaShanZheng-Regular.ttf"
            size 45
        elif _preferences.language == "chinese":
            font "fonts/chi_genkai/Genkaimincho.ttf"
            size 45
    text [nation.leadersub] xalign 0.28 yalign 0.25 size 13 font "fonts/eng_moria/MoriaCitadel.ttf":
        if _preferences.language == "chinesesim":
            font "fonts/chi_cities/MaShanZheng-Regular.ttf"
            size 30
        elif _preferences.language == "chinese":
            font "fonts/chi_genkai/Genkaimincho.ttf"
            size 30
    text [__("Government Type:")]+[nation.politicalID] xalign 0.9 yalign 0.45 size 15 font "fonts/eng_moria/MoriaCitadel.ttf":
        if _preferences.language == "chinesesim":
            font "fonts/chi_cities/MaShanZheng-Regular.ttf"
            size 40
        elif _preferences.language == "chinese":
            font "fonts/chi_weidong/wts11.ttf"
            size 40
    text [__("Political Alignment:")]+[nation.AlignmentID] xalign 0.9 yalign 0.5 size 15 font "fonts/eng_moria/MoriaCitadel.ttf":
        if _preferences.language == "chinesesim":
            font "fonts/chi_cities/MaShanZheng-Regular.ttf"
            size 40
        elif _preferences.language == "chinese":
            font "fonts/chi_weidong/wts11.ttf"
            size 40
    text [__("Ruling Party:")]+[nation.rulingparty] xalign 0.9 yalign 0.55 size 15 font "fonts/eng_moria/MoriaCitadel.ttf":
        if _preferences.language == "chinesesim":
            font "fonts/chi_cities/MaShanZheng-Regular.ttf"
            size 40
        elif _preferences.language == "chinese":
            font "fonts/chi_weidong/wts11.ttf"
            size 40
    text [__("Alliances:")]+[nation.factionID] xalign 0.9 yalign 0.6 size 15 font "fonts/eng_moria/MoriaCitadel.ttf":
        if _preferences.language == "chinesesim":
            font "fonts/chi_cities/MaShanZheng-Regular.ttf" 
            size 40 
        elif _preferences.language == "chinese":
            font "fonts/chi_weidong/wts11.ttf"
            size 40
    add nation.flagimg xalign 0.9 yalign 0.1 at guo_flag_pulse
    imagebutton:
        idle "gui/button_return_icon.png"
        hover "gui/button_return_icon_hovered.png"
        xalign 0.96
        action Return()
    vbox xalign 0.3 ypos 300 xsize 600 ysize 1000 box_wrap True:
            text nation.info size 20 font "fonts/chi_genkai/Genkaimincho.ttf":
                if _preferences.language == "chinesesim":
                    font "fonts/chi_cities/MaShanZheng-Regular.ttf"



screen guo_map():
    tag menu
    key "K_SPACE" action ShowMenu(main_menu)
    key "K_TAB" action ShowMenu(main_menu)
    key "K_ESCAPE" action Return()
    key "5" action ShowMenu ('guo')
    key "n" action Return()
    key "1" action Return()
    key "o" action ShowMenu('political_menu')
    key "g" action ShowMenu('glossary')
    key "p" action ShowMenu('poems')
    key "l" action ShowMenu('historical_event_log')
    key "2" action ShowMenu('glossary')
    key "3" action ShowMenu('poems')
    key "4" action ShowMenu('historical_event_log')
    on "hide":
        action Stop("music", fadeout=2.5)
    viewport:
        xysize (config.screen_width, config.screen_height)
        child_size (4040, 2230)
        mousearea: # For dyanmic sounds
            area (3065, 827, 975, 1403)
            hovered [SetScreenVariable("seaside", True), Play("sound", "sounds/map/ambient_seaside.ogg", loop=True)]
            unhovered [SetScreenVariable("seaside", False), Stop("sound",fadeout=2.5)]
        mousearea: # For dyanmic sounds
            area (2108, 1881, 1959, 349)
            hovered [SetScreenVariable("seaside", True), Play("sound", "sounds/map/ambient_seaside.ogg", loop=True)]
            unhovered [SetScreenVariable("seaside", False), Stop("sound", fadeout=2.5)]
        if show_beijing:
            xinitial 2300
            yinitial 500
        else:
            xinitial 2300
            yinitial 500
        draggable True
        edgescroll (700, 700)
        add "maps/GUO_map.png"
        for q in TL_GUO_loc:
            $ nx = q.x +5
            $ ny = q.y -12
            if q.IsActive:
                for n in TL_GUO:
                    if n.ID ==q.ID:
                        $ act = SetVariable('nation', n)
                button:
                    xpos nx
                    ypos ny
                    text q.name color "#000000" hover_color "#FF0000" size 20:
                        if _preferences.language == "chinesesim":
                            font"fonts/chi_cities/MaShanZheng-Regular.ttf" 
                        elif _preferences.language == "chinese":
                            size 25
                    action [act, Stop("sound", fadeout=2.5), Show('guo')]
                if not q.Port and not q.Capital:
                    add "gui/map_bullet.png" xpos q.x ypos q.y
                if q.Port and not q.Capital:
                    add "gui/map_port.png" xpos q.x ypos q.y
                if q.Capital and not q.Port:
                    add "gui/map_bullet_capital.png" xpos q.x ypos q.y
                if q.Capital and q.Port:
                    add "gui/map_bullet_capital.png" xpos q.x ypos q.y
                    
screen glossary():
    default show_return = False
    default return_button_img = renpy.random.randint(1,3)
    key "g" action [Return()]
    key "2" action [Return()]
    key "K_ESCAPE" action Return()
    key "n" action ShowMenu('guo_map')
    key "p" action ShowMenu('poems')
    key "l" action ShowMenu('historical_event_log')
    key "1" action ShowMenu('guo_map')
    key "3" action ShowMenu('poems')
    key "4" action ShowMenu('historical_event_log')
    tag menu
    window:
        style "gm_root"
        add "#000c" 
    text __("Glossary") size 40 xalign 0.5 ypos 20
    timer 3.0 action [SetScreenVariable("show_return", True)]
    showif show_return:
        showif return_button_img ==1:
            add "00_keyboard_prompts/Dark/Keyboard_Black_2.png" xalign 0.9 yalign 0.1 at fader
        showif return_button_img ==2:
            add "00_keyboard_prompts/Dark/Keyboard_Black_G.png" xalign 0.9 yalign 0.1 at fader
        showif return_button_img ==3:
            add "00_keyboard_prompts/Dark/Keyboard_Black_Esc.png" xalign 0.9 yalign 0.1 at fader
    hbox spacing 200:
        viewport:
            xpos 50 ypos 100 xsize 300 ysize 500
            #child_size (None, 4000)
            scrollbars "vertical"
            spacing 5
            draggable True
            mousewheel True
            arrowkeys True
            add "#000c"
            vbox spacing 20:
            # loop over persistent here
                for word in sorted(persistent.unlocked):
                    textbutton word:
                           action SetVariable("display_desc", word)
        vbox ypos 100 xsize 650 ysize 500 box_wrap True:
            text glossary_dict.get(display_desc, "")


screen poems():
    key "p" action Return()
    key "3" action Return()
    key "K_ESCAPE" action Return()
    key "n" action ShowMenu('guo_map')
    key "g" action ShowMenu('glossary')
    key "l" action ShowMenu('historical_event_log')
    key "1" action ShowMenu('guo_map')
    key "2" action ShowMenu('glossary')
    key "4" action ShowMenu('historical_event_log')
    tag menu
    window:
        style "gm_root"
        add "#000c" 
    text "Poems" size 40 xalign 0.5 ypos 20
    hbox spacing 200:
        viewport:
            xpos 50 ypos 100 xsize 300 ysize 500
            child_size (None, 4000)
            scrollbars "vertical"
            spacing 5
            draggable True
            mousewheel True
            arrowkeys True
            vbox spacing 20:
                # use sorted(glossary_dict.keys()) instead of glossary_dict
                # to arrange the terms in alphabetic order
                for poem in sorted(persistent.unlocked_poem):
                    textbutton poem:
                        action SetVariable("poem_display_desc", poem)
        vbox ypos 100 xsize 650 ysize 500 box_wrap True:
            text poem_dict.get(poem_display_desc, "")


default event_display_bg = 0

image historical_event_background = ConditionSwitch(
    "event_display_bg == 1", "history_log_bgs/Marco_polo_bridge_bg.png",
    "True", "history_log_bgs/default_history_bg.png")

screen historical_event_log():
    tag menu
    key "l" action Return()
    key "4" action Return()
    key "K_ESCAPE" action Return()
    key "n" action ShowMenu('guo_map')
    key "g" action ShowMenu('glossary')
    key "p" action ShowMenu('poems')
    key "1" action ShowMenu('guo_map')
    key "2" action ShowMenu('glossary')
    key "3" action ShowMenu('poems')
    window:
        style "gm_root"
        add "historical_event_background"
    text __("Historical Events") size 40 xalign 0.5 ypos 20
    hbox spacing 200:
        viewport:
            xpos 50 ypos 100 xsize 300 ysize 500
            child_size (None, 4000)
            scrollbars "vertical"
            spacing 5
            draggable True
            mousewheel True
            arrowkeys True
            vbox spacing 20:
                for  num,event in enumerate(persistent.unlocked_history):
                    textbutton event:
                        action [SetVariable("event_display_desc", event), SetVariable("event_display_bg", num)]
                        selected event_display_desc == event and event_display_bg == num
                        sensitive event_display_desc != event and event_display_bg != num
        vbox ypos 100 xsize 650 ysize 500 box_wrap True:
            text historical_event_log.get(event_display_desc, "")